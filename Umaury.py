import numpy as np
from scipy.optimize import curve_fit, minimize_scalar
from scipy.integrate import quad
from scipy.special import erfcx

import Rb
λV = 532e-9

Emaury = np.array([1.36545,
                   1.23697,
                   1.14242,
                   1.07082,
                   1.00870,
                   .943052,
                   .874535,
                   .805109,
                   .735360,
                   .665471,
                   .595509,
                   .525509,
                   .455485,
                   .385446,
                   .315398,
                   .245343,
                   .175284,
                   .105221,
                   .0351563])
Er = 8.11e3 # en Hz
Emaury *= Er
nmaury = np.arange(2, 21)

g = 9.80927
νB = g * Rb.m * λV / (2 * Rb.h)
a = λV / 2
def CPfit(n, A, c, νB=-νB):
    return A / n**4 + n * νB + c

p0 = 1, 0#, -νB
maury_fit, dfit = curve_fit(CPfit, nmaury[-8:], Emaury[-8:], p0=p0)


def Umaury(n):
    n = np.asarray(n, dtype=float)
    return np.piecewise(n, [np.logical_and(n < 20, n >=2), n < 2],
                        [ lambda n: Emaury[n.astype(int) - 2],
                         float('nan'),
                         #lambda n: Emaury[-1] -(Emaury[-2] - Emaury[-1]) * (n - 20)])
                         lambda n: CPfit(n, *maury_fit)])


def gauss_cloud(x, x0, σ):
    return 1 / np.sqrt(2 * np.pi * σ**2) * np.exp(- (x - x0)**2 / (2 * σ**2))

def meanCP_bloch(m, xc, σcloud, tau_pi=22e-3, T=.1, δAC=0):
    """
    Force de CP moyenne mesurée en faisant deux mesures à ±m transitions, moyennée sur la taille du nuage
    """
    f1 = meanCP_f0(m, xc, σcloud, tau_pi, T, δAC)
    f2 = meanCP_f0(-m, xc, σcloud, tau_pi, T, δAC)

    f_bloch = (f2 - f1) / (2 * m)
    return f_bloch

def meanCP_f0(m, xc, σcloud, τπ, T, δAC=0):
    """
    Fréquence centrale d'un interféromètre sous l'influnce de la force de CP
    """

    f0 = Umaury(1000 + m) - Umaury(1000)
    xc = np.atleast_1d(xc)
    fCP = np.zeros(len(xc))

    for ix0 in (np.argsort(xc)[::-1]):
        x0 = xc[ix0]
        Pe = lambda f: meanCP_interf(m, f, σcloud, x0, τπ, T, δAC)
        f0 = integrate_fringe(Pe, f0, 1/(T + 2 * τπ / np.pi) / 4, N=40)[-1]
        #f0 = integrate_fringe(Pe, f0, 1/T / 4, N=50)[-1]
        fCP[ix0] = f0

    return fCP

def meanCP_bloch2(m, xc, σcloud, tau_pi=22e-3, T=.1, δAC=0):
    """
    Force de CP moyenne mesurée en faisant deux mesures à ±m transitions, moyennée sur la taille du nuage
    """
    f1 = meanCP_f02(m, xc, σcloud, tau_pi, T, δAC)
    f2 = meanCP_f02(-m, xc, σcloud, tau_pi, T, δAC)

    f_bloch = (f2 - f1) / (2 * m)
    return f_bloch

def meanCP_f02(m, xc, σcloud, τπ, T, δAC=0):
    """
    Fréquence centrale d'un interféromètre sous l'influnce de la force de CP
    """

    f0 = Umaury(1000 + m) - Umaury(1000)
    xc = np.atleast_1d(xc)
    fCP = np.zeros(len(xc))

    sweep = 1/(T + 2 * τπ / np.pi) / 4
    for ix0 in (np.argsort(xc)[::-1]):
        x0 = xc[ix0]
        Pe = lambda f: -meanCP_interf(m, f, σcloud, x0, τπ, T, δAC)
        f0 = minimize_scalar(Pe, (f0 - sweep, f0, f0 + sweep), tol=1e-7).x
        fCP[ix0] = f0

    return fCP

def P_interf(δ, Ω0, τ, T, δAC):
    """
    Probabilité d'excitation d'un interféromètre π/2 - π/2, en présence d'un LS raman 
    différentiel δAC 
    """

    φ = 0
    Ωr = np.sqrt((δ - δAC)**2 + Ω0**2)
    return 4 * Ω0**2 / Ωr**2 * np.sin(Ωr * τ / 2)**2 \
        * (np.cos(Ωr * τ / 2) * np.cos((δ * T + φ) / 2) - (δ - δAC) / Ωr * np.sin(Ωr * τ / 2) * np.sin((δ * T + φ) / 2))**2

def meanCP_interf(m, f, σcloud, xc, τπ, T, δAC=0): 

    nrange = np.arange(2 - min(0, m), int((np.max(xc) + 3 * σcloud) / a))[None, :]
    μ = gauss_cloud(nrange * a, xc, σcloud)

    f = np.atleast_1d(f)
    δ = 2 * np.pi * (f[:, None] - (Umaury(nrange + m) - Umaury(nrange)))
    Pe = P_interf(δ, np.pi / τπ, τπ/2, T, δAC * 2 * np.pi)

    return np.sum(Pe * μ, -1) / np.sum(μ)

def integrate_fringe(Pe, f0, sweep, G=.79, N=10):
    """
    Intégration de la frange centrale
    """
    Pe0 = Pe(f0 + sweep)
    f = np.empty(N)
    for i in range(N):
        f[i] = f0
        Pe0, Pe1 = Pe(f0 - (-1)**i * sweep), Pe0
        f0 -= (Pe0 - Pe1) * G * (-1)**i

    return f

def P_rabi(δ, Ω0, τ, T, δAC):
    """
    Probabilité d'excitation rabi d'un interféromètre π/2 - π/2, en présence d'un LS raman 
    différentiel δAC 
    """
    φ = 0
    Ωr = np.sqrt((δ - δAC)**2 + Ω0**2)
    return 4 * Ω0**2 / Ωr**2 * np.sin(Ωr * τ / 2)**2 \
        * (np.cos(Ωr * τ / 2)**2 - ((δ - δAC) / Ωr * np.sin(Ωr * τ / 2))**2)**2


μ = 3 * 3.33e-30 # Moment dipolaire du Rb sur de la silice
α0 = .079e-4
def gauss_dip(d, N, σ0) :
    #A = np.exp(d**2 / (2 * σ0**2)) * erfc(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    A = erfcx(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    dE = N**2 * μ**2 * (-2 * d * σ0 + A * (d**2 + σ0**2)) * (-2 * σ0 * (d**2 + 2 * σ0**2) + d * A * (d**2 + 3 * σ0**2)) / (64 * np.pi**2 * Rb.ε0**2 * σ0**12)
    F = Rb.h * α0 * dE

    return F * 532e-9 / (2 * Rb.h) #- gauss_dip(350e-6, N, σ0)

def _gauss_dip_z_mean(d, N, σ0, σc):
    low_lim = np.maximum(0, d - 4 * σc)
    if low_lim > 0:
        norm = 1
    else:
        norm = quad(lambda z: 1 / np.sqrt(2 * np.pi * σc**2) * np.exp(-(z - d)**2 / (2 * σc**2)),
                low_lim, d + 4 * σc)[0]
            
    return quad(lambda z: 1 / np.sqrt(2 * np.pi * σc**2) * np.exp(-(z - d)**2 / (2 * σc**2)) * gauss_dip(z, N, σ0),
                low_lim, d + 4 * σc)[0] / norm

gauss_dip_z_mean = np.vectorize(_gauss_dip_z_mean, otypes=[float])

def Egauss(d, N, σ0) :
    A = erfcx(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    E = N * μ * (-2 * d * σ0 + A * (d**2 + σ0**2)) / (8 * np.pi * Rb.ε0 * σ0**5)

    return E

def dEgauss(d, N, σ0) :
    """
    Gradient de champ électrique
    """
    #A = np.exp(d**2 / (2 * σ0**2)) * erfc(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 

    A = erfcx(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    dE = N * μ * (-2 * σ0 * (d**2 + 2 * σ0**2) + d * A * (d**2 + 3 * σ0**2)) / (8 * np.pi * Rb.ε0 * σ0**7)

    return dE


def FEgauss(d, N, σ0):
    F = Rb.h * α0 * Egauss(d, N, σ0) * dEgauss(d, N, σ0)
    return F * 532e-9 / (2 * Rb.h) #- gauss_dip(350e-6, N, σ0)

