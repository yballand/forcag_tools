# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 16:09:49 2022

@author: administrator
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.optimize import curve_fit

def sort_plot(x, y, *args, **kargs):
    mask = ~ (np.isnan(x) | np .isnan(y))
    x_sort, y_sort = zip(*sorted(zip(x[mask], y[mask])))
    plt.plot(x_sort, y_sort, *args, **kargs)

data = pd.read_csv('calibrage puissance detection fluo.dat', sep='\t')

sort_plot(data.iloc[:, 0], data.iloc[:, 1], '+-')
plt.xlabel(data.columns[0])
plt.ylabel(data.columns[1])
plt.locator_params(nbins=10, axis='y')

plt.grid(True, 'both')
plt.yticks(np.arange(0, 2.4, .25))

#plt.legend()
plt.title("Calibration de la réponse de l'AOM cooling\n(01/2022)")
plt.savefig('Calibration P détection.png', dpi=300)

###############
#plt.figure(figsize=(8,6))

#x = data.iloc[:, 1]
#y = data.iloc[:, 2]
#mask = ~ (np.isnan(x) | np .isnan(y))
#
#def s_fit(x, Isat, a):
#    s = x / Isat
#    return a * s / (1 + s)
#    
#
#param, var = curve_fit(s_fit, x[mask], y[mask], p0 = (1, 2e6))
#
#x = np.linspace(0, 2.5, 100)
#
#sort_plot(data.iloc[:, 1], data.iloc[:, 2], '+-', label=data.columns[2])
#plt.xlabel(data.columns[1])
#sort_plot(data.iloc[:, 1], -data.iloc[:, 3], '+-', label=data.columns[3])
#plt.plot(x, s_fit(x, *param), label='saturation fit, Psat={:.2}'.format(param[0]))
#
#plt.legend()
#plt.title("Calibration de la réponse de l'AOM")
#
#
#plt.figure(figsize=(8,6))
#sort_plot(data.iloc[:, 1], data.iloc[:, 4], '+-', label='y')
#sort_plot(data.iloc[:, 1], data.iloc[:, 5], '+-', label='x')
#plt.xlabel('Puissance fluo (mW)')
#plt.ylabel('Pixel centre nuage')
#plt.legend(loc = 5)
plt.show()
#
#
#    
#    
