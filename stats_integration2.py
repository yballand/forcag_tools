from datetime import datetime, timezone
from functools import reduce, cache, partial
from pathlib import Path
import re
from collections.abc import Iterable

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import pandas as pd

import allantools
import forcag_tools
import calibration_miroir
import Rb

# A difference in these keys is not significant to distinguish configs
allowed_key_diff = {'dds2_f1', 
                    'integrator_center', 
                    'integrator_Rfluctuations',
                    'integrator_relaxed_Rfluctuations',
                    'standby',
                    'resize',
                    'integrator_min_N_rejection',
                    'SavedThe',
                    } # TODO prendre en compte les champs non pertinents de la config bloch

# A difference in these keys is significant only if there the only ones
optionnal_key_diff = {'integrator_gain', 
                      'integrator_sweep', 
                      }

nu_g = 568.509003

plt.rcParams['figure.constrained_layout.use'] = True

cvi_epoch = datetime(1900, 1, 1, 0, 0, tzinfo=timezone.utc)
def cvitime2datetime(time):
    delta = np.timedelta64(np.array(time), 's')

    return cvi_epoch + delta 

def sqrt_fit(x, A):
    return A / np.sqrt(x)

def fit_adev(allan, allan_taus=None):
    if allan_taus == 'all':
        mask = (allan['tau'] > 1e2) & (allan['tau'] < 1e3)
    else:
        mask = slice(3, 7)
    return curve_fit(sqrt_fit, allan['tau'][mask], allan['adev'][mask])[0][0]

def couple_bloch_config(config1, config2):
    """
    Check if the two configs are made to measure the same Bloch config by a differential measurement

    Return None if they differs, and the number of bloch frequency measured else.
    """
    if config1 is config2:
        return False

    diff = config1.items() ^ config2.items()

    diff_keys = {e[0] for e in diff}
    
    if "bloch_config" in diff_keys:
        b1 = config1['bloch_config']
        b2 = config2['bloch_config']
        if np.abs(forcag_tools.compute_bloch(b1.param_seqs[0], b1.freq) -
                  forcag_tools.compute_bloch(b2.param_seqs[0], b2.freq)) < 10e-9 :
            diff_keys.remove("bloch_config")

    return all(k in allowed_key_diff for k in diff_keys)
        #return None

def find_couple_config(df_configs):
    config_couples = ([[couple_bloch_config(c1, c2) for c2 in df_configs['config']] for c1 in df_configs['config']]) 
    #print(couple_bloch_config(configs[27], configs[28]))
    list_couples = []

    for i in range(len(df_configs.config)):
        if sum(config_couples[i]) == 1:
            j = config_couples[i].index(True)
            if j < i:
                list_couples.append(None)
            elif sum(config_couples[j]) == 1:
                list_couples.append(df_configs.iloc[j]['n_config'])
            else:
                list_couples.append(df_configs.iloc[i]['n_config'])
        else:
            list_couples.append(df_configs.iloc[i]['n_config'])

    df_configs['couple'] = list_couples
    return df_configs
    return pd.Series(list_couples, index=df_configs.index.droplevel(0))


def nb_bloch_couple_config(config1, config2):
    """Return the number of bloch frequency measured in a differential measurment"""

    bloch_f = 568
    
    if config1['integrator_type'] == 1: # Raman integration
        n_bloch_osc = round((config1['integrator_center'] - config2['integrator_center']) 
                            / bloch_f)
    elif config1['integrator_type'] == 2: # MW integration
        n_bloch_osc = round((config1['dds2_f1'] - config2['dds2_f1']) / bloch_f)

    else:
        n_bloch_osc = 1

    return n_bloch_osc

def process_data(data_dir, misc=None, plot=False, couples=True, allan_taus=None):
    data_dir = [Path(data_d) for data_d in data_dir]

    if misc is not None:
        if not len(misc) == len(data_dir):
            raise ValueError(f'misc if given must be of equal length ({len(misc)}) than data_dir ({len(data_dir)})')

    else:
        misc = [None] * len(data_dir)

    # test which config where used
    lock_re = re.compile(r'^lock(\d+)_') 
    configs_used = []
    dates = []

    for data_d, misc_desc in zip(data_dir, misc):
        list_configs = []
        for file in data_d.glob('lock*_data.dat'):
            if match := lock_re.match(file.name):
                list_configs.append({'path': data_d,
                                  'n_config': int(match.group(1)),
                                  'misc': misc_desc})

        if list_configs:
            try:
                dates.append(datetime.fromtimestamp((data_d / 'config0').stat().st_mtime))
            except FileNotFoundError:
                dates.append(datetime.fromtimestamp((data_d / 'config0.ini').stat().st_mtime))

            configs_used.append(pd.DataFrame(list_configs).sort_values('n_config').set_index('n_config', drop=False))

    if not configs_used:
        raise ValueError(f"No integration in path {data_dir}")

    configs_used = pd.concat(configs_used, keys=dates, names=['date'])

    paths = [path / f'config{n_config}' for path, n_config in zip(configs_used['path'], configs_used['n_config'])]
    configs_used['config'] = forcag_tools.parse_config(*paths)
    configs_used['cycle_time'] = [c['cycle'] * 1e-3 for c in configs_used['config']]

    # Test all the appariements to find config pairs in a given config
    list_couples = configs_used.groupby(level=0).apply(find_couple_config)
    #config_couples = ([[couple_bloch_config(c1, c2) for c2 in configs_used['config']] for c1 in configs_used['config']]) 
    ##print(couple_bloch_config(configs[27], configs[28]))
    #list_couples = []

    #if couples:
    #    for i in range(len(configs_used.config)):
    #        if sum(config_couples[i]) == 1:
    #            j = config_couples[i].index(True)
    #            if j < i:
    #                list_couples.append(None)
    #            elif sum(config_couples[j]) == 1:
    #                list_couples.append(configs_used.iloc[j]['n_config'])
    #            else:
    #                list_couples.append(configs_used.iloc[i]['n_config'])
    #        else:
    #            list_couples.append(configs_used.iloc[i]['n_config'])


    #configs_used['couple'] = list_couples.T
    configs_used = list_couples

    # parse monitors data
    data_raw = []
    for idx, inte_cfg in configs_used.iterrows():
        m = pd.read_csv(inte_cfg.path / f"lock{inte_cfg.n_config}_monitoring.dat", index_col='number',
                        delim_whitespace=True, usecols=['aire0', 'aire1', 'ratio', 'utc', 'number'])

        d = pd.read_csv(inte_cfg.path / f"lock{inte_cfg.n_config}_data.dat", delim_whitespace=True,
                        index_col=0, names=['correction'])



        if not d.empty:
            d['freq'] = inte_cfg.config['integrator_center'] + d['correction']
            #if inte_cfg.config['integrator_type'] == 2: #MW
            #    d['freq'] = inte_cfg.config['dds2_f1'] - d['freq'] # TODO take into account phase shift due to pi/2 pulses

            data_raw.append(m.join(d, how='inner'))

    data_raw = pd.concat(data_raw, keys=configs_used.index)
    data_raw['Nat'] = data_raw['aire0'] + data_raw['aire1'] 
    data_raw['utc'] = pd.to_datetime(data_raw['utc'], unit='s', origin=pd.Timestamp('1900-01-01'), utc=True)

    data_raw.index.set_names(['date', 'n_config', 'number'], inplace=True)
    data_raw['couple'] = configs_used['couple']
    configs_used['utc_start'] = data_raw['utc'].groupby(['date', 'n_config']).first()


    # Creation of the couples
    data_couple = data_raw[['freq', 'couple']].join(data_raw[['freq']], on=['date', 'couple', 'number'],
                                                    how='inner', lsuffix='_c1', rsuffix='_c2')
    configs_couple = configs_used.join(configs_used['config'], on=['date', 'couple'],
                                       how='inner', lsuffix='_c1', rsuffix='_c2')

    configs_couple['n_bloch'] = [nb_bloch_couple_config(c1, c2) for c1, c2 in
                                 zip(configs_couple['config_c1'], configs_couple['config_c2'])]

    if plot:
        # We should need to name the configs only when directly plotting them
        config_names = configs_couple['config_c1'].apply(config_name, args=[configs_used['config']]) 
        # Si on a des doublons dans les noms de config, on en reprend des un peu plus détaillés
        if not config_names.is_unique:
            config_names = configs_couple['config_c1'].apply(config_name, args=[configs_used['config'], False]) 

        configs_couple['config_name'] = config_names 

    data_couple['n_bloch'] = configs_couple['n_bloch']

    #data_couple['bloch_f'] = (data_couple.freq_c1 - data_couple.freq_c2) / data_couple.n_bloch
    #data_couple['center_f'] = data_couple[['freq_c1', 'freq_c2']].mean(axis=1)
    bloch_f = (data_couple.freq_c1 - data_couple.freq_c2) / data_couple.n_bloch
    center_f = data_couple[['freq_c1', 'freq_c2']].mean(axis=1)
    data_f = pd.concat([bloch_f, center_f], keys=['bloch_f', 'center_f'], names=['type_f'], axis=1).stack()
    data_f = data_f.swaplevel().sort_index()
    data_f.name = 'f'

    configs_couple = pd.concat([configs_couple, configs_couple], keys=['bloch_f', 'center_f'], names=['type_f'])
    configs_couple = configs_couple.reorder_levels(['date', 'n_config', 'type_f']).sort_index()

    # Hauteur de lancer
    h = []
    for config in configs_couple['config_c1']:
        bloch_c = config['bloch_config']
        h.append(forcag_tools.compute_bloch(bloch_c.param_seqs[0], bloch_c.freq))
    configs_couple['h'] = h
    configs_couple['d'] = (forcag_tools.ref_miroir - configs_couple['h']).round(8)
    # Stats
    stats_f = data_f.groupby(level=['date', 'n_config', 'type_f'], sort=False).apply(compute_stats, configs_couple, allan_taus=allan_taus).unstack()

    data_raw_stats = data_raw.groupby(level=['date', 'n_config'])
    stats_f['ratio_mean'] = data_raw_stats['ratio'].mean()
    stats_f['Nat_mean'] = data_raw_stats['Nat'].mean()
    configs_couple = configs_couple.join(stats_f)

    stats_raw = data_raw.freq.groupby(level=['date', 'n_config']).apply(compute_stats, configs_used, allan_taus=allan_taus).unstack()
    configs_used = configs_used.join(stats_raw)

    if plot:
        plot_Nat(configs_used, data_raw)
        plot_correction(configs_used, data_raw)

        plot_freq(configs_couple, data_f)
        plot_allan(configs_couple)
        plot_fft(configs_couple, data_f)

        mir_fit, mir_fit_agg, s = mirror_calibration(configs_used)
        if mir_fit is not None:
            print(s.d_mir * 1e6)
            plot_mir_d(mir_fit)


    #mirror_calibration(configs_couple)
    return configs_couple, data_f, configs_used, data_raw


def compute_stats(bloch_f, configs=None, cycle_time=None, allan_taus=None):
    mean = bloch_f.mean()

    if not cycle_time:
        if configs is None:
            cycle_time = 1 #row['config1']['cycle'] * 1e-3
        else:
            config = configs.loc[bloch_f.name]
            cycle_time = config['cycle_time']

            # If differential measurement, cycle time is twice as big
            if 'n_bloch' in config.index and config.n_bloch != 0:
                cycle_time *= 2

    allan = allan_dev(bloch_f, rate=1/(cycle_time), allan_taus=allan_taus)

    allan['mean'] = mean
    allan['nb_shot'] = len(bloch_f)
    allan['time_shot'] = len(bloch_f) * cycle_time
    allan['std_fit'] = fit_adev(allan, allan_taus)
    allan['est_std_allan'] = allan['adev'][-1]
    allan['est_std_fit'] = sqrt_fit(len(bloch_f) * cycle_time, allan['std_fit'])
    allan['est_std_fit_tau'] = sqrt_fit(allan.tau.max(), allan['std_fit'])
    allan['est_std_normal'] = bloch_f.std() / np.sqrt(len(bloch_f))
    allan['std_f'] = bloch_f.std()
    #allan['Nat_mean'] = bloch_f
    
    return allan

def allan_dev(df, rate=1, allan_taus=None):
    #print(df)
    allan = allantools.oadev(df.to_numpy(), data_type="freq", rate=rate, taus=allan_taus)
    allan = pd.Series(allan, index=('tau', 'adev', 'adeverr', 'n'))

    # Cf https://github.com/amv213/Stable32-AllanTools/blob/master/Stable87.py
    # et https://www.anderswallin.net/2016/11/allantools-2016-11-now-with-confidence-intervals/
    
    lo_l = []
    hi_l = []
    for (m, adev) in zip(np.round(allan.tau * rate), allan.adev):
        edf = edf_greenhall(alpha=0, # Supposition d'un bruit blanc en fréquencelbh
                                       d=2,     # allan dev
                                       m = m,
                                       N = len(df),
                                       overlapping=True,
                                       modified=False)

        #lo, hi = allantools.confidence_interval(dev=adev, edf=edf)
        lo, hi = confidence_interval(edf=edf)
        lo_l.append(lo * adev)
        hi_l.append(hi * adev)

    allan['adeverr_lo'] = allan.adev - lo_l
    allan['adeverr_hi'] = hi_l - allan.adev
                                                              
    return allan

edf_greenhall = cache(allantools.edf_greenhall)
confidence_interval = cache(partial(allantools.confidence_interval, 1))

def mirror_calibration(config_couple): 
    calib_mir = {}
    for data_d in config_couple.path.unique():
        # Retrieve the data for mirror calibration
        try:
            calib_mir[data_d] = pd.read_csv(data_d / 'mirror_calibration.dat', sep='\t')
        except FileNotFoundError as e:
            print(e)

    if not calib_mir:
        return None, None, config_couple

    mir_data = pd.concat(calib_mir.values(), keys=calib_mir.keys(), names=['path'])

    mir_data['h'] = (mir_data.Nbloch)
    mir_data['datetime'] = pd.to_datetime(mir_data.datetime)

    mir_fit = mir_data.groupby(['path', 'datetime']).apply(calibration_miroir.mir_data_fit)
    mir_fit[mir_fit.A < 2e5] = np.nan
    mir_fit = mir_fit.dropna()

    mir_fit_agg = mir_fit[['x0', 'dx']].groupby(['path'], sort=False).agg(['mean', 'std'])
    mir_fit_count = mir_fit[['x0', 'dx']].groupby(['path'], sort=False).count()

    idx = pd.IndexSlice
    #mir_fit_agg.loc[:, idx[:, 'std']] /= np.sqrt(mir_fit_count)
    mir_fit_agg.columns = ['_'.join(col) for col in mir_fit_agg.columns]
    #mir_fit_agg['x0_std'] /= (len(mir_fit_count))**.35 # Moins qu'une racine carrée, pour prendre en compte les fluctuations long termes
    #mir_fit_agg['x0_std'] /= np.sqrt(len(mir_fit_count))

    h = []
    for config in config_couple['config']:
        bloch_c = config['bloch_config']
        h.append(forcag_tools.compute_bloch(bloch_c.param_seqs[0], bloch_c.freq))
    config_couple['h'] = h

    config_couple = config_couple.join(mir_fit_agg, on='path')
    config_couple['d_mir'] = config_couple.x0_mean - config_couple.h 

    return mir_fit, mir_fit_agg, config_couple


def config_name(config, list_configs, skip_otionnal=True):
    """
    Find a human readable and pertinant name for a given config.

    Will find all the differences with other config to find 
    a distinguishable name
    """
    diff_keys = set()

    diff_keys.update(*[{key for key, item in config.items() ^ conf.items()} for conf in list_configs])
    diff_keys -= allowed_key_diff

    # Remove optionnal key difference only if there is something else
    if skip_otionnal and diff_keys - optionnal_key_diff:
        diff_keys -= optionnal_key_diff

    def find_field_name(config, list_configs):
        diff_keys = set()

        diff_keys.update(*[{key for key, item in config.items() ^ conf.items()} for conf in list_configs])
        diff_keys -= allowed_key_diff

        # Remove optionnal key difference only if there is something else
        if skip_otionnal and diff_keys - optionnal_key_diff:
            diff_keys -= optionnal_key_diff

        diff_fields = {}
        for key in diff_keys:
            if key in config:
                if key == 'bloch_config':
                    launch_h = forcag_tools.compute_bloch(config[key].param_seqs[0], config[key].freq) 
                    diff_fields['d mir'] = np.around((forcag_tools.ref_miroir - launch_h) * 1e6, 2)
                elif not isinstance(config[key], tuple):
                    diff_fields[key] = config[key]
                else:
                    try:
                        key_config = config[key]._asdict()
                        list_key_configs = [c[key]._asdict() for c in list_configs]
                    except AttributeError:
                        key_config = dict(enumerate(config[key]))
                        try:
                            list_key_configs = [dict(enumerate(c[key])) for c in list_configs]
                        except KeyError:
                            #print(f"Key {key} not found")
                            list_key_configs = []
                    diff_fields.update(find_field_name(key_config, list_key_configs))
        return diff_fields 

    diff_fields = find_field_name(config, list_configs)
    name = ', '.join(sorted(f'{k}={v}' for k, v in diff_fields.items()))
    return (name[:60] + '…') if len(name) > 60 else name


    diff_fields = {}
    for key in diff_keys:
        if key in config:
            if not isinstance(config[key], tuple):
                diff_fields[key] = config[key]
            else:
                # difference in a tuple, find which elements really differ
                diff_idx = set()
                diff_idx.update(*[{i for i in range(len(config[key])) if config[key][i] != c[key][i]}
                                  for c in list_configs])
                for i in diff_idx:
                    try:
                        idx_name = config[key]._fields[i]
                        kname = f"{idx_name}" # We make the assumption that in the wfm case, only one field will change
                    except AttributeError:
                        kname = f"{key}[{i%10}]"

                    diff_fields[kname] = config[key][i]

    return ', '.join(sorted(f'{k}={v}' for k, v in diff_fields.items()))

def plot_Nat(config, monitor):
    # Gestion des coups ratés à faire.
    plt.figure(4)
    plt.clf()
    fig, (ax, ax1) = plt.subplots(2, 1, num=4, constrained_layout=True, sharex=True)

    for n_config, m in monitor.groupby(level=['date', 'n_config']):
        conf_name = config.loc[n_config, 'path'] / f"config{config.loc[n_config, 'n_config']}"
        ax.plot(m['utc'], m['Nat'], '.', label=conf_name)

    ax.legend()

    ax.set_xlabel('N coup')
    ax.set_ylabel('Nat')

    for n_config, m in monitor.groupby(level=['date', 'n_config']):
        conf_name = config.loc[n_config, 'path'] / f"config{config.loc[n_config, 'n_config']}"
        ax1.plot(m['utc'], m['ratio'], '.', label=conf_name)

    ax1.legend()

    ax1.set_xlabel('N coup')
    ax1.set_ylabel('ratio')

def plot_correction(config, data):
    fig, ax = plt.subplots(num=6)
    plt.clf()

    fig, (ax, ax2) = plt.subplots(2, 1, num=6)

    for i, (n_config, m) in enumerate(data.groupby(level=['date', 'n_config'])):
        ax.plot(m.index.get_level_values('number'), m['correction'], f'-C{i%10}', label=f"config {n_config[1]}")
        ax.axhline(m['correction'].mean(), ls='--', c=f'C{i%10}')


    ax.legend()

    ax.set_xlabel('N coup')
    ax.set_ylabel('Correction apportée')

    for i, (n_config, m) in enumerate(data.groupby(level=['date', 'n_config'])):
        ax2.plot(m['utc'], m['correction'], f'-C{i%10}', label=f"config {n_config[1]}")
        ax2.axhline(m['correction'].mean(), ls='--', c=f'C{i%10}')


    ax2.legend()

    ax2.set_xlabel('N coup')
    ax2.set_ylabel('Correction apportée')

def plot_freq(config, data):
    type_f = data.index.get_level_values('type_f').unique()
    #data_couple = data[data.index.isin(['bloch_f', 'center_f'], level='type_f')]
    #data_single = data[data.index.isin(['single_f'], level='type_f')]

    if 'bloch_f' in type_f:
        fig, ax = plt.subplots(num=5)
        plt.clf()

        fig, (ax1, ax2) = plt.subplots(2, 1, num=5)

        ax1.axhline(nu_g, ls=':',  c='k')
        #for i, (couple, d) in enumerate(data_couple.groupby(level=0)):
        #    r = result[(result['n couple'] == couple) & (result['type'] == 'bloch f')].iloc[0]
        #    ax1.plot(d.index.get_level_values('number'), d['bloch f'], '-', label=r['config_name'])
        #    ax1.axhline(r['mean'], ls='--', color=f'C{i%10}')

        data_bloch = data.xs('bloch_f', level='type_f', drop_level=False)
        for i, (idx, dataf) in enumerate(data_bloch.groupby(level=['date', 'n_config', 'type_f'])):
            ax1.plot(dataf.index.get_level_values('number'), dataf, '-', label=config.loc[idx, 'config_name'])
            ax1.axhline(config.loc[idx, 'mean'], ls='--', color=f'C{i%10}')

        ax1.legend()

        ax1.set_xlabel('N coup')
        ax1.set_ylabel('Bloch frequency (Hz)')

        data_center = data.xs('center_f', level='type_f', drop_level=False)
        for i, (idx, dataf) in enumerate(data_center.groupby(level=['date', 'n_config', 'type_f'])):
            ax2.plot(dataf.index.get_level_values('number'), dataf, '-', label=config.loc[idx, 'config_name'])
            ax2.axhline(config.loc[idx, 'mean'], ls='--', color=f'C{i%10}')

        ax2.legend()

        ax2.set_xlabel('N coup')
        ax2.set_ylabel('Center frequency (Hz)')


    if 'single_f' in type_f:
        fig, ax = plt.subplots(num=8)
        plt.clf()

        fig, ax1 = plt.subplots(1, 1, num=8)

        data_single = data.xs('single_f', level='type_f', drop_level=False)
        for i, (idx, dataf) in enumerate(data_single.groupby(level=['date', 'n_config', 'type_f'])):
            ax1.plot(dataf.index.get_level_values('number'), dataf, '-', label=config.loc[idx, 'config_name'])
            ax1.axhline(config.loc[idx, 'mean'], ls='--', color=f'C{i%10}')

        ax1.legend()

        ax1.set_xlabel('N coup')
        ax1.set_ylabel('Center frequency (Hz)')

def plot_allan(result):
    type_f = result.index.get_level_values('type_f').unique()

    if 'bloch_f' in type_f:
        result = result.drop('misc', axis=1).dropna()

        fig, ax = plt.subplots(num=7)
        plt.clf()
        fig, axs = plt.subplots(2, 1, num=7, constrained_layout=True, sharex=True)

        max_tau = max(max(r) for r in result['tau'])
        min_tau = min(min(r) for r in result['tau'])
        t = np.linspace(min_tau, max_tau)
        for type_f, ax in zip(('bloch_f', 'center_f'), axs):
            if type_f in result.index.get_level_values('type_f'):
                data = result.xs(type_f, level='type_f', drop_level=False)
                for k, i in enumerate(data.itertuples()):
                    ax.errorbar(i.tau, i.adev, i.adeverr, capsize=3, label=i.config_name, color=f'C{k}')
                    ax.plot(t, sqrt_fit(t, i.std_fit), lw=.4, c=f'C{k}')


            ax.set_xscale('log')
            ax.set_yscale('log')

            ax.grid(True, which="minor", ls="-", color='0.65')
            ax.grid(True, which="major", ls="-", color='0.25')

            ax.legend()

        axs[0].set_ylabel('Bloch frequency (Hz)')
        axs[1].set_ylabel('Shift frequency (Hz)')
        axs[1].set_xlabel('Averaging time (s)')

        fig.suptitle('Overlapping Allan deviation')

        
    if 'single_f' in type_f:
        fig, ax = plt.subplots(num=9)
        plt.clf()
        fig, ax = plt.subplots(1, 1, num=9, constrained_layout=True)


        max_tau = max(max(t for t in r) for r in single_f['tau'])
        min_tau = min(min(t for t in r) for r in single_f['tau'])
        t = np.linspace(min_tau, max_tau)
        single_f = result.xs('single_f', level='type_f', drop_level=False)
        for k, i in enumerate(single_f.itertuples()):
            ax.errorbar(i.tau, i.adev, i.adeverr, capsize=3, label=i.config_name, color=f'C{k}')
            ax.plot(t, sqrt_fit(t, i.std_fit), lw=.4, c=f'C{k}')

        ax.set_xlabel('Averaging time (s)')

        ax.set_xscale('log')
        ax.set_yscale('log')

        ax.grid(True, which="minor", ls="-", color='0.65')
        ax.grid(True, which="major", ls="-", color='0.25')


        ax.legend()

        ax.set_ylabel('Peak frequency (Hz)')
        ax.set_xlabel('Averaging time (s)')

        fig.suptitle('Overlapping Allan deviation')

def plot_fft(config, data):
    plt.figure(1)
    plt.clf()

    N = 4

    fig, axs = plt.subplots(2, 1, num=1)

    for type_f, ax in zip(('bloch_f', 'center_f'), axs):
        if type_f in data.index.get_level_values('type_f'):
            data_bloch = data.xs(type_f, level='type_f', drop_level=False)
            for i, (idx, dataf) in enumerate(data_bloch.groupby(level=['date', 'n_config', 'type_f'])):
                cycle_time = config.loc[idx, 'cycle_time']
                freq = np.fft.rfftfreq(dataf.size, cycle_time)[1:]
                fft = np.fft.rfft(dataf)[1:]
                fft_df = pd.DataFrame({'fft': np.abs(fft),
                                       'freq': freq
                                       }).rolling(N).mean()
                fft_df['T'] = 1 / fft_df.freq
                ax.plot(fft_df.freq, fft_df.fft, '-', label=config.loc[idx, 'config_name'])
            #ax.plot(freq, np.convolve(np.abs(fft), np.ones(N) / N), '-', label=config.loc[idx, 'config_name'])

        ax.legend()

        ax.set_xlabel('freq')

    axs[0].set_ylabel('Bloch frequency (Hz) fft')
    axs[1].set_ylabel('Center frequency (Hz) fft')

def plot_mir_d(mir_fit):
    plt.figure(10)
    plt.clf()
    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, num=10, sharex=True)
    ref_fit = mir_fit[mir_fit.dx0 < 100e-6]
    dt = ref_fit.index.get_level_values('datetime')
    ax1.errorbar(dt, (ref_fit.x0 - forcag_tools.ref_miroir) * 1e6, ref_fit.dx0 * 1e6, capsize=3)
    ax2.errorbar(dt, ref_fit.dx * 1e6, ref_fit.ddx * 1e6, capsize=3)
    ax3.errorbar(dt, ref_fit.A, ref_fit.dA, capsize=3)

    ax1.set_ylabel(f'Position miroir - {forcag_tools.ref_miroir} (µm)')
    ax2.set_ylabel(f'Largeur nuage (µm)')
    ax1.set_xlabel('Moment mesure')

def config_reference_raw(config_couple, data_couple, ref_query, groupby=None, op=np.subtract, drop_index=()):
    """
    Apply a differential measurement, with a config given by the reference ref

    """

    config_diff = []
    datas_diff = {}

    index_names = config_couple.index.names
    droped_index =  ['date', 'n_config']
    droped_index.extend(drop_index)

    if not groupby:
        groupby = 'date'
    elif isinstance(groupby, str):
        groupby = [groupby, 'date']
    else:
        groupby = [*groupby, 'date']

    ref_exists = False
    for (group, couple) in config_couple.groupby(groupby, as_index=False):
        ref_config = reduce(np.logical_and, (couple[key] == value for key, value in ref_query.items()))
        ref_idx = ref_config.index[ref_config]

        group_index = couple.index[~ref_config]
        if ref_config.any():
            ref_exists = True

        if ref_config.any() and not group_index.empty:
            config_diff.append(couple[~ref_config])

            # a bit of advanced indexing
            # Because ref_config and data_config do not share the same index length, we cannot use classical indexing
            data_loc = tuple(group_index)[0]
            ref_loc = tuple(ref_idx)
            data_group = data_couple.loc[data_loc]
            data_ref = data_couple.loc[ref_loc]

            data_diff = op(data_group, data_ref)#.droplevel(droped_index))
            datas_diff[data_loc] = (data_diff.dropna())#.reorder_levels([*index_names, ''])) # Pourquoi l'ordre fluctue ?


    if not ref_exists:
        raise ValueError('No reference config found')

    config_diff = pd.concat(config_diff)
    datas_diff = pd.concat(datas_diff, names=['date', 'n_config'])# names=[*groupby[:-1]])


    #datas_diff = datas_diff.reorder_levels(['date', 'n_config', 'type_f', 'number']).sort_index()
    stats_f = datas_diff.groupby(level=index_names, sort=False).apply(compute_stats, config_diff).unstack()
    stats_f = stats_f.astype({'mean': float, 'est_std_allan': float})
    config_diff = config_diff[config_diff.columns.difference(stats_f.columns)].join(stats_f)

    return config_diff.sort_index(), datas_diff


def config_reference(config_couple, data_couple, ref_query, groupby=None, op=np.subtract, drop_index=()):
    """
    Apply a differential measurement, with a config given by the reference ref

    """

    config_diff = []
    datas_diff = []

    index_names = config_couple.index.names
    droped_index =  ['date', 'n_config']
    droped_index.extend(drop_index)

    if not groupby:
        groupby = 'date'
    elif isinstance(groupby, str):
        groupby = [groupby, 'date']
    else:
        groupby = [*groupby, 'date']


    ref_exists = False
    for (group, couple) in config_couple.groupby(groupby, as_index=False):
        ref_config = reduce(np.logical_and, (couple[key] == value for key, value in ref_query.items()))
        ref_idx = ref_config.index[ref_config]

        group_index = couple.index[~ref_config]
        if ref_config.any():
            ref_exists = True

        if ref_config.any() and not group_index.empty:
            config_diff.append(couple[~ref_config])

            # a bit of advanced indexing
            # Because ref_config and data_config do not share the same index length, we cannot use classical indexing
            data_loc = tuple(list(i) for i in zip(*group_index))
            ref_loc = tuple(list(i) for i in zip(*ref_idx))
            data_group = data_couple.loc[data_loc]
            data_ref = data_couple.loc[ref_loc]

            data_diff = op(data_group, data_ref.droplevel(droped_index))
            datas_diff.append(data_diff.dropna().reorder_levels([*index_names, 'number'])) # Pourquoi l'ordre fluctue ?


    if not ref_exists:
        raise ValueError('No reference config found')

    config_diff = pd.concat(config_diff)
    datas_diff = pd.concat(datas_diff, names=[*groupby[:-1]])

    #datas_diff = datas_diff.reorder_levels(['date', 'n_config', 'type_f', 'number']).sort_index()
    stats_f = datas_diff.groupby(level=index_names, sort=False).apply(compute_stats, config_diff).unstack()
    stats_f = stats_f.astype({'mean': float, 'est_std_allan': float})
    config_diff = config_diff[config_diff.columns.difference(stats_f.columns)].join(stats_f)

    return config_diff.sort_index(), datas_diff

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Processing of ForcaG integration data")
    parser.add_argument("dir", help="integration data path", nargs='+')
    args = parser.parse_args()

    r = process_data(args.dir, plot=True)
    for dat in r[2].itertuples():
        print(f'Config {dat.n_config} : f0 = {dat.mean: .3f} ± {dat.est_std_allan:.3f} Hz')

    plt.show()
