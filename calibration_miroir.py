import argparse
from collections import namedtuple
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt 
from scipy.optimize import curve_fit, fsolve
from scipy.special import erfc
import pandas as pd

import forcag_tools 

plt.rcParams['figure.constrained_layout.use'] = True

def erf_fit(x, A, x0, dx, c=0):
    return A * erfc((x - x0) / (np.sqrt(2) * dx)) / 2 + c

FitResult = namedtuple('FitResult', ('A', 'x0', 'dx', 'c'))

def inv_bloch_seq(x):
    return [fsolve(lambda n: forcag_tools.standard_bloch_seq(n) - x0 * 1e-6, 235.15)[0] for x0 in np.atleast_1d(x)]

def fit_data(data_file):
    data_nbloch = pd.read_csv(data_file, sep='\t', index_col=0, skiprows=0).dropna()

    data_nbloch['h'] = (data_nbloch.Nbloch)
    data_nbloch['datetime'] = pd.to_datetime(data_nbloch.datetime)

    if 'datetime' in data_nbloch:
        return data_nbloch, data_nbloch.groupby('datetime').apply(mir_data_fit)
                

    p0 = (data_nbloch.Nat.max(), data_nbloch.h.mean(), 4e-6, 0)
    fit, cov_fit = curve_fit(erf_fit, data_nbloch['h'], data_nbloch['Nat'], p0=p0)
    dfit = np.sqrt(np.diag(cov_fit)) 

    return data_nbloch, (FitResult(*fit), FitResult(*dfit))

p0 = (2.5e6, .30245, 3e-6, 0)

def mir_data_fit(data):
    if len(data) <= 4:
        return 

    try:
        p0 = (data.Nat.max(), data.h.mean(), 4e-6, 0)
        fit, cov_fit = curve_fit(erf_fit, data['h'], data['Nat'], p0=p0)
        dfit = np.sqrt(np.diag(cov_fit)) 
    except RuntimeError:
        fit = p0
        dfit = np.zeros_like(fit)
    fit_list = dict(zip(('A', 'x0', 'dx', 'c'), fit))
    fit_list.update(dict(zip(('dA', 'dx0', 'ddx', 'dc'), dfit)))
    return pd.Series(fit_list)


def plot(data_nbloch, fit):
    plt.figure(1)
    plt.clf()
    fig, ax = plt.subplots(num=1)

    for i, (dt, data_n) in enumerate(data_nbloch.groupby('datetime')): 
        ncount = data_n.groupby('h').agg({'Nat': ['mean', 'std']})

        x = np.linspace(ncount.index.min(), ncount.index.max(), 100) 
        ax.errorbar(ncount.index * 1e6 - 302430, ncount['Nat', 'mean'], yerr=ncount['Nat', 'std'], capsize = 3, label=dt, color=f'C{i%10}')

        ax.plot(x * 1e6 - 302430, erf_fit(x, *fit.loc[dt, ['A', 'x0', 'dx', 'c']]), f'--C{i%10}')
    ax.set_ylabel(f'Nat signal')
    ax.set_xlabel('Distance lancer')
    ax.grid(True)
    #ax.legend() 


    sec_ax = ax.secondary_xaxis('top', functions=(inv_bloch_seq, lambda n: forcag_tools.standard_bloch_seq(n) * 1e6))
    sec_ax.set_ylabel('Nbloch')

    #Nmirror = inv_bloch_seq(fit[1] * 1e6)[0]
    #ax.set_title(
    #    f'Position du miroir : {fit[1]*1e6:.7} ± {dfit[1] * 1e6 * 2:.2}μm (N{Nmirror:.7}) \n'
    #    f'taille du nuage : {fit[2] * 1e6:.2} ± {dfit[2] * 2e6:.2} µm\n'
    #    )

    plt.figure(2)
    plt.clf()
    fit.A[fit.A < 2e5] = np.nan
    fit.dx0[fit.dx0 > 100e-6] = np.nan
    fit = fit.dropna()
    fig, (ax1, ax2, ax3) = plt.subplots(3, num=2, sharex=True)

    ax1.errorbar(fit.index, fit.x0 * 1e6 - 302430, fit.dx0 * 1e6, capsize=3)
    ax1.set_ylabel(f'Position du miroir (µm)')
    ax1.grid(True)

    #sec_ax = ax1.secondary_yaxis('right', functions=(inv_bloch_seq, lambda n: forcag_tools.standard_bloch_seq(n) * 1e6))
    #sec_ax.set_ylabel('Nbloch')

    #ax2.errorbar(fit.index, fit.dx * 1e6, fit.ddx * 1e6, capsize=3)
    ax2.plot(fit.index, fit.dx * 1e6)
    ax2.set_ylabel(f'Taille du nuage (µm)')
    ax2.grid(True)

    ax3.plot(fit.index, fit.A)
    ax3.set_ylabel('Amplitude signal')
    ax3.grid(True)

    ax3.set_xlabel('Heure mesure')

    return fig

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Miror launch height calibration')
    parser.add_argument('file', metavar='calibration file')
    args = parser.parse_args()

    ncount, fit = fit_data(args.file)

    plot(ncount, fit)
    #f, df = fit
    #Nmirror = inv_bloch_seq(f[1] * 1e6)[0]

    #fit.A[fit.A < 5e4] = np.nan
    fit.dx0[fit.dx0 > 100e-6] = np.nan
    fit = fit.dropna()
    print(
        f'Position du miroir : {fit.x0.mean()*1e6:.7} ± {fit.x0.std() * 1e6 * 2:.3}μm\n'
        f'taille du nuage : {fit.dx.mean() * 1e6:.2} ± {fit.dx.std() * 2e6:.2} µm\n'
    )

    plt.show()
