from collections import namedtuple
import configparser
from pathlib import Path
from datetime import datetime
from itertools import chain
import subprocess

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pycparser
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
from PIL import Image
import allantools

import header_parser
import Rb
import __main__
import os

RESULTS = Path('~/syrte2/forcag/Results/').expanduser()
results = RESULTS

DEFINITIONS = "Definitions.h"
GIT_DIR = "~/syrte2/yballand/ForcaG"
#GIT_DIR = "~/OBSP/ForcaG"
GIT_BRANCH = "master"


GRAVITY = 9.80928
ref_miroir = 302430e-6

def parse_config(*config_paths):
    res = []
    for p in config_paths:
        p = Path(p)
        if not p.suffix == '.ini' and p.exists():
            res.extend(r._asdict() for r in parse_bin_config(p))
        else:
            res.extend(parse_ini_config(p.with_suffix('.ini')))

    return res

memoize_parser = {}
def parse_bin_config(*config_paths):
    """
    Parse a binary config (such as saved in an integration),
    through a parsing of the Definitions.h header
    """
    config_path = Path(config_paths[0])

    if not config_path.is_file():
        raise ValueError(f"Config file {str(config_path)} does not exist")

    mtime = datetime.fromtimestamp(config_path.stat().st_mtime)

    last_commit = subprocess.check_output(["git", "-C", Path(GIT_DIR).expanduser(), "rev-list", f"{GIT_BRANCH}", "-1", "--before", mtime.isoformat()])
    last_commit_str = last_commit.decode().strip()
    definitions = subprocess.Popen(["git", "-C", Path(GIT_DIR).expanduser(), "show", f"{last_commit_str}:{DEFINITIONS}"], stdout=subprocess.PIPE)

    definitions_preprocess = subprocess.check_output(["clang", "-E", "-"],
                                                     stdin=definitions.stdout)
    if definitions_preprocess in memoize_parser:
        hardware_struct = memoize_parser[definitions_preprocess]
    else: 
        
        parser = pycparser.CParser()
        ast = parser.parse(definitions_preprocess.decode().replace('DWORD', 'unsigned long'),
                           DEFINITIONS)

        hardware_ast = header_parser.FindStruct().find(ast, 'hardware_config_type') 
        hardware_struct = header_parser.parse_struct_ast(hardware_ast, ast)

        memoize_parser[definitions_preprocess] = hardware_struct

    return [header_parser.parse_struct(c_path, hardware_struct) for c_path in config_paths]

def convert_ini_type(value):
    try:
        return float(value)
    except ValueError:
        value = value.strip('"')
        if ',' in value:
            return tuple(float(e) for e in value.split(',') if not e.isspace())
            # test nécessaire pour contourner le problème des chaines de caractères sur deux lignes
        else:
            return value

def parse_ini_config(*config_paths):
    """
    Parse an ini config file (such as saved in an integration),
    """
    cfg = configparser.ConfigParser()
    cfg.optionxform = lambda option: option # do not ignore case

    res_configs = []
    for path in config_paths:
        cfg.read(path)
        res_dict = dict(chain(*[cfg.items(section) for section in cfg]))

        # Conversion en float ou en liste de float
        res_dict = {key: convert_ini_type(value) for key, value in res_dict.items()}

        res_dict2 = {}
        for key in res_dict:
            if 'Line000' in key:
                key1 = key.split()[0]
                res_dict2[key1] = res_dict[key1 + ' Line0001'] + res_dict[key1 + ' Line0002']
            elif key.startswith('pulse') or key == 'mw_2':
                res_dict2[key + '_delay'], res_dict2[key + '_duration'] = res_dict[key][:2]
                if len(res_dict[key]) > 2:
                    res_dict2[key + '_state'] = res_dict[key][2]
                if len(res_dict[key]) > 3:
                    res_dict2[key + '_polarity'] = res_dict[key][3]
            elif key.startswith('dds_') and key :
                mode = key.removeprefix('dds_')
                if mode == 'clearphase':
                    continue
                for i in range(4):
                    res_dict2[f'dds{i+1}_{mode}'] = res_dict[key][i]
            elif key == 'ao':
                for i in range(4):
                    res_dict2[f'ao_{i}'] = res_dict[key][i]

            else:
                res_dict2[key] = res_dict[key]

        param_seqs = [{key: convert_ini_type(value) for key, value in cfg.items(f'PARAMS SEQUENCE {i}')}
                      for i in range(1, 6)]
        param_seqs_tuple = namedtuple('param_seq', param_seqs[0])

        bloch_config = {
            'seq_switchs': tuple(cfg['SWITCH SEQUENCES'][f'SEQ_{i}'] for i in range(1, 6)),
            'com_port': cfg['DDS programming']['COM port'], 
            'ramp_rate': cfg['DDS programming']['RampRate'],
	#PARAM_SEQ 'param_seqs'[BLOCH_NB_SEQ];
            'Tdelay': cfg['DDS programming']['Tdelay'],
            'freq': res_dict2['freq'],
            'param_seqs': tuple(param_seqs_tuple(**param_seq) for param_seq in param_seqs),
        }
        bloch_config_tuple = namedtuple('bloch_config', bloch_config)
        res_dict2['bloch_config'] = bloch_config_tuple(**bloch_config)
        res_configs.append(res_dict2)

    return res_configs

def compute_bloch(param_seq, laser_freq):

    # Conversion from namedtuples
    if hasattr(param_seq, '_asdict'):
        param_seq = param_seq._asdict()

    g = param_seq.get('g', GRAVITY)

    k = 2 * np.pi * laser_freq / Rb.c
    hbar =  1.054571596e-34
    vrec = (hbar * k / Rb.m)
    nurec = Rb.hbar * k**2 / (4 * np.pi * Rb.m)

    Nbloch = [param_seq['Nramp1'], param_seq['Na1'], param_seq['Nff'], param_seq['Nramp2'], param_seq['Nramp3'], param_seq['Na2'], param_seq['Nramp4']]
    Tbloch = np.array([param_seq['Tramp1'], param_seq['Ta1'], param_seq['Tff'], param_seq['Tramp2'], param_seq['Tramp3'], param_seq['Ta2'], param_seq['Tramp4']]) * 1e-3


    vf = 0
    zf = 0
    for i in range(len(Nbloch)):
        if Tbloch[i]:
            a = Nbloch[i] * 2 * vrec / Tbloch[i] - g
            zf += a * Tbloch[i]**2 / 2 + vf * Tbloch[i]
            vf += a * Tbloch[i]

    return zf

def standard_bloch_seq(Nbloch):
    """Compute the launching height for the standard Bloch sequence currently used, 
    where only the inital number of bloch oscillations change"""
    seq = {'g': 9.81, 'Tramp1': 0.6, 'Nramp1': 0.0, 'Ta1': 6.0, 'Na1': 234.7, 'Tff': 0.2, 'Nff': 0.0, 'Tramp2': 105.5, 'Nramp2': 89.0, 'Tramp3': 0.2, 'Nramp3': 0.0, 'Ta2': 6.0, 'Na2': -224.0, 'Tramp4': 0.6, 'Nramp4': 0.0, 'DF1': 0, 'DF2': 0, 'T_BRAGG': 0.0, 'N_BRAGG': 0, 'Offset_BRAGG': 0.0}
    freq = 383704.4e9

    seq['Na1'] = Nbloch
    return compute_bloch(seq, freq)
seq = {'g': 9.81,
       'Tramp1': 0.6,
       'Nramp1': 0.0,
       'Ta1': 6.0,
       'Na1': 209.212,
       'Tff': 0.6,
       'Nff': 0.5,
       'Tramp2': 119.086,
       'Nramp2': 99.3996,
       'Tramp3': 0.6,
       'Nramp3': 0.5,
       'Ta2': 6.0,
       'Na2': -198.194,
       'Tramp4': 0.6,
       'Nramp4': 0.0,
       'DF1': 0,
       'DF2': 0,
       'T_BRAGG': 0.0,
       'N_BRAGG': 0,
       'Offset_BRAGG': 0.0}
# Routines d'aide
def open_tiff_files(directory):
    """
    Open all tiff files in the given directory and return the numpy array with the pixel values
    """

    images = []
    list_im = Path(directory).glob('*.tiff')
    for f in sorted(list_im, key=lambda f: f.stat().st_mtime):
        im = Image.open(f)
        im_tiff = []
        for i in range(3):
            im.seek(i)
            im_tiff.append(np.array(im))

        images.append(im_tiff)

    return np.array(images)


_raman_interp = None 
def calibration_raman(U):
    global _raman_interp
    if not _raman_interp:
        file_path = Path(__file__).parent / 'calibrations/Raman 20220412.dat'
        raman_data = pd.read_csv(file_path, sep='\t')

        mean_raman = np.sqrt(raman_data['Raman 2 top chamber (µW)'] *  
                             raman_data['Raman 2 top chamber (µW)']) * 1e-6

        _raman_interp = interp1d(raman_data['U Raman (V)'], mean_raman, kind='cubic')

    return _raman_interp(U)

_IR_interp = {}
def calibration_IR(U, old=False):
    global _IR_interp
    if old not in  _IR_interp:
        if old:
            file_path = Path(__file__).parent / 'calibrations/IR 20220404.dat'
        else:
            file_path = Path(__file__).parent / 'calibrations/nlle calibration IR courant pompe 15A 20220721.dat'
        ir_data = pd.read_csv(file_path, sep='\t')

        # Negative tensions
        _IR_interp[old] = interp1d(-ir_data.iloc[:, 0].abs(), ir_data.iloc[:, 1], kind='cubic')

    result = _IR_interp[old](U)
    if result.size == 1:
        return result.item()
    return result

_det_interp = None 
def calibration_det(U):
    global _det_interp
    if not _det_interp:
        file_path = Path(__file__).parent / 'calibrations/det 20220128.dat'
        det_data = pd.read_csv(file_path, sep='\t').groupby('U AOM (V)').mean()

        _det_interp = interp1d(det_data.index, det_data['P fluo (mW)'] * 1e-3, kind='cubic')

    result = _det_interp(U)
    if result.size == 1:
        return result.item()
    return result

λ780 = 780.24e-9
def LShift2level(λ, I0):
    """
    Light shift computed in the simple case of a 2 level atom
    """

    Γ = 2 * np.pi * 6.06e6
    Isat = 16.69

    s = I0 / Isat
    Δ2 = 2 * np.pi * Rb.c * (1 / λ - 1 / λ780)

    return 1/4 * Rb.hbar * Γ**2 / (2 * Δ2) * s

def LShift(λ, I0):
    λ795 = 794.76e-9
    λ420 = 420.18e-9
    λ421 = 421.55e-9
    
    ωHFS = 2 * np.pi * 6.834e9

    D1 = 2.537e-29
    D2=2.534e-29
    D3=2e-30
    D4=2e-30

    E0 = np.sqrt(2 * I0 / (Rb.ε0 * Rb.c))
    Ec = E0 / 2
    Ω1 = D1 * Ec / Rb.hbar 
    Ω2 = D2 * Ec / Rb.hbar 
    Ω3 = D3 * Ec / Rb.hbar 
    Ω4 = D4 * Ec / Rb.hbar 

    λs = np.array([λ795, λ780, λ421, λ420])
    Δ1, Δ2, Δ3, Δ4 = 2 * np.pi * Rb.c * (1/λ - 1/λs)
    Δ1c, Δ2c, Δ3c, Δ4c = 2 * np.pi * Rb.c * (1/λ + 1/λs)

    U1 = Rb.hbar * (4/6 * Ω2**2 * (1/Δ2 - 1/Δ2c) +
                    4/12 * Ω1**2 * (1/Δ1 - 1/Δ1c) +
                    4/6 * Ω4**2 * (1/Δ4 - 1/Δ4c) +
                    4/12 * Ω3**2 * (1/Δ3 - 1/Δ3c))

    return U1

def LShiftDiff(λ, I0):
    I0 = np.asarray(I0)
    λ780 = 780.24e-9
    λ795 = 794.76e-9
    λ420 = 420.18e-9
    λ421 = 421.55e-9
    
    ωHFS = 2 * np.pi * 6.834e9

    D1 = 2.537e-29
    D2=2.534e-29
    D3=2e-30
    D4=2e-30

    E0 = np.sqrt(2 * I0 / (Rb.ε0 * Rb.c))
    Ec = E0 / 2
    Ω1 = D1 * Ec / Rb.hbar 
    Ω2 = D2 * Ec / Rb.hbar 
    Ω3 = D3 * Ec / Rb.hbar 
    Ω4 = D4 * Ec / Rb.hbar 

    λs = np.array([λ795, λ780, λ421, λ420])
    Δ1, Δ2, Δ3, Δ4 = 2 * np.pi * Rb.c * (1/λ - 1/λs)
    Δ1c, Δ2c, Δ3c, Δ4c = 2 * np.pi * Rb.c * (1/λ + 1/λs)

    U1 = Rb.hbar * (4/6 * Ω2**2 * (1/Δ2 - 1/Δ2c) +
                    4/12 * Ω1**2 * (1/Δ1 - 1/Δ1c) +
                    4/6 * Ω4**2 * (1/Δ4 - 1/Δ4c) +
                    4/12 * Ω3**2 * (1/Δ3 - 1/Δ3c))

    U2 = Rb.hbar * (4/6 * Ω2**2 * (1/(Δ2-ωHFS) - 1/(Δ2c+ωHFS)) +
                    4/12 * Ω1**2 * (1/(Δ1-ωHFS) - 1/(Δ1c+ωHFS)) +
                    4/6 * Ω4**2 * (1/(Δ4-ωHFS) - 1/(Δ4c+ωHFS)) +
                    4/12 * Ω3**2 * (1/(Δ3-ωHFS) - 1/(Δ3c+ωHFS)))
    return U2 - U1


def sqrt_fit(t, A):
    return A / np.sqrt(t)


def plot_allan(result, num=None):
    plt.figure(num)
    plt.clf()

    if isinstance(result, pd.DataFrame):
        max_tau = max(max(r) for r in result['tau'])
        min_tau = min(min(r) for r in result['tau'])
        t = np.linspace(min_tau, max_tau)
        for k, i in enumerate(result.itertuples()):
            plt.errorbar(i.tau, i.adev, i.adeverr, capsize=3, label=f'data {k}', color=f'C{k%10}')
            plt.plot(t, sqrt_fit(t, i.std_fit), lw=.4, c=f'C{k%10}')

    else:
        max_tau = max(result['tau'])
        min_tau = min(result['tau'])
        t = np.linspace(min_tau, max_tau)
        plt.errorbar(result.tau, result.adev, result.adeverr,
                    capsize=3)
        plt.plot(t, sqrt_fit(t, result.std_fit), lw=.4)


    plt.xscale('log')
    plt.yscale('log')

    plt.grid(True, which="minor", ls="-", color='0.65')
    plt.grid(True, which="major", ls="-", color='0.25')

    plt.legend()

    plt.ylabel('Allan dev')
    plt.xlabel('Averaging time (s)')

def plot_data(result, fig_num=None, key=None):
    plt.figure(fig_num)
    plt.clf()

    if key is not None:
        result = result.loc[key.name]
        plt.plot(np.arange(len(result)), result)
        return


    for i, (k, dat) in enumerate(result.groupby(result.index.names[:-1])):
        plt.plot(np.arange(len(dat)), dat, c=f'C{i%10}')

def P_interf(δ, Ω0, τ, T, δAC=0):
    """
    Probabilité d'excitation d'un interféromètre π/2 - π/2, en présence d'un LS raman 
    différentiel δAC 
    """

    φ = 0
    Ωr = np.sqrt((δ - δAC)**2 + Ω0**2)
    return 4 * Ω0**2 / Ωr**2 * np.sin(Ωr * τ / 2)**2 \
        * (np.cos(Ωr * τ / 2) * np.cos((δ * T + φ) / 2) - (δ - δAC) / Ωr * np.sin(Ωr * τ / 2) * np.sin((δ * T + φ) / 2))**2

def P_interf2(δ, Ω0, τ, T, δAC=0):
    """
    Probabilité d'excitation d'un interféromètre π/2 - π/2, en présence d'un LS raman 
    différentiel δAC 
    """

    φ = 0
    Ωr = np.sqrt((δ - δAC)**2 + Ω0**2)
    return 4 * Ω0**2 / Ωr**2 * np.sin(Ωr * τ / 2)**2 \
        * (np.cos(Ωr * τ / 2)**2 + ((δ - δAC) / Ωr * np.sin(Ωr * τ/2))**2) \
        * np.cos((δ * T + φ) / 2 + np.arctan((δ - δAC) / Ωr * np.tan(Ωr * τ / 2)))**2  
        #* (1 + np.cos(δ * T + φ + 2 * ((δ - δAC) / Ωr))) / 2

def E_interf(δ, Ω0, τ, T, δAC=0):
    """
    Enveloppe de la probabilité d'excitation d'un interféromètre π/2 - π/2, en présence d'un LS raman 
    différentiel δAC 
    """

    φ = 0
    Ωr = np.sqrt((δ - δAC)**2 + Ω0**2)
    return 4 * Ω0**2 / Ωr**2 * np.sin(Ωr * τ / 2)**2 \
        * (np.cos(Ωr * τ / 2)**2 + ((δ - δAC) / Ωr * np.sin(Ωr * τ/2))**2)

def P_e(δ, Ω0, τ, δAC=0):
    """
    Probabilité d'excitation d'un pulse spectroscopique, en présence d'un LS raman 
    différentiel δAC 
    """

    Ωr = np.sqrt((δ - δAC)**2 + Ω0**2)
    return Ω0**2 / Ωr**2 * np.sin(Ωr * τ / 2)**2 

def to_csv(df, filename, float_decimal=4, col_sep='\t'):
    if isinstance(df, dict):
        df = pd.DataFrame(df)
        df = df.set_index(df.columns[0])

    with open(filename, 'w') as f:
        filename = getattr(__main__, '__file__', os.getcwd())
        f.write(f'#{filename}\n')
        df.to_csv(f, sep=col_sep, float_format=f'%.{float_decimal}e', na_rep='nan')


def allan_dev(data, rate, taus='octave'):
    allan = allantools.oadev(np.asarray(data), data_type="freq", rate=rate, taus=taus)
    allan = pd.DataFrame(np.asarray(allan).T, columns=('tau', 'adev', 'adeverr', 'n'))

    # Cf https://github.com/amv213/Stable32-AllanTools/blob/master/Stable87.py
    # et https://www.anderswallin.net/2016/11/allantools-2016-11-now-with-confidence-intervals/
    
    lo_l = []
    hi_l = []
    for (m, adev) in zip(np.round(allan.tau * rate), allan.adev):
        edf = allantools.edf_greenhall(alpha=0, # Supposition d'un bruit blanc en fréquencelbh
                                       d=2,     # allan dev
                                       m = m,
                                       N = len(data),
                                       overlapping=True,
                                       modified=False)

        #lo, hi = allantools.confidence_interval(dev=adev, edf=edf)
        lo, hi = allantools.confidence_interval(adev, edf=edf)
        lo_l.append(lo)
        hi_l.append(hi)

    allan['adeverr_lo'] = allan.adev - lo_l
    allan['adeverr_hi'] = hi_l - allan.adev

    mask = slice(0, int(np.sqrt(len(data))))
    fit, dfit = curve_fit(sqrt_fit, allan.tau[mask], allan.adev[mask],
                          sigma=allan.adeverr[mask], absolute_sigma=True)

    allan['std_fit'] = sqrt_fit(allan.tau, *fit)
                                                              
    return allan
    
def eval_uncertainty(model, x, fit, cov_fit):
    """Évalue l'incertitude sur la fonction modèle en fonction des paramètres trouvés par curve_fit"""

    std_params = np.sqrt(np.diag(cov_fit))
    # Détermination des dérivées
    dscale =1e-2
    list_dparams = []
    for iparam in len(fit):
        param = np.array(fit)
        dparam = std_params[i] * dscale
        param[iparam] += dparam
        v1 = model(x, *param)
        param[iparam] += -2 * dparam
        v2 = model(x, *param)

        list_dparams.append((v1 - v2) / (2 * dparam))

    list_dparams = np.asarray(list_dparams)[None, :]

    return list_dparams.T @ cov_fit @ list_dparams
