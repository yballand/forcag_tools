from functools import partial
import numpy as np
from scipy.optimize import curve_fit, minimize_scalar
from scipy.special import erfcx, kve, hyperu
from scipy.integrate import quad, trapezoid

#from numba import jit, float64, int64, vectorize

import Rb

rng = np.random.default_rng(0)

λV = 532e-9

# Table III.4 thèse Maury
Emaury = np.array([[1.36545, 9.4e-5],
                   [1.23697, 8.9e-5],
                   [1.14242, 8e-5  ], 
                   [1.07082, 5.4e-5], 
                   [1.00870, 1.5e-5], 
                   [.943052, 5.6e-6], 
                   [.874535, 3.1e-6], 
                   [.805109, 1.3e-6], 
                   [.735360, 1.9e-6], 
                   [.665471, 2e-6  ], 
                   [.595509, 1.3e-6], 
                   [.525509, 9e-7  ], 
                   [.455485, 10e-7 ], 
                   [.385446, 7e-7  ], 
                   [.315398, 7e-7  ], 
                   [.245343, 6e-7  ], 
                   [.175284, 5e-7  ], 
                   [.105221, 5e-7  ], 
                   [.0351563, 4.5e-7]])
Er = 8.11e3 # en Hz
Emaury *= Er
nmaury = np.arange(2, 21)

g = 9.80927
νB = g * Rb.m * λV / (2 * Rb.h)
νB = 0.0700738 * Er # p. 63 thèse Maury. Attention, utilisation de Er = 8.11kHz, ne donne pas la bonne valeur de la gravité
a = λV / 2
def CPfit4(n, A, c, νB=-νB):
    return A / n**4 + n * νB + c

p0 = 1, 0#, -νB
maury_fit4, dfit = curve_fit(CPfit4, nmaury[-6:], Emaury[-6:, 0], p0=p0, sigma=Emaury[-6:, 1], absolute_sigma=True)
maury_dfit4 = np.sqrt(np.diag(dfit))

def CPfit3(n, A, c, νB=-νB):
    return A / n**3 + n * νB + c
maury_fit3, dfit = curve_fit(CPfit3, nmaury[-6:], Emaury[-6:, 0], p0=p0, sigma=Emaury[-6:, 1], absolute_sigma=True)
maury_dfit3 = np.sqrt(np.diag(dfit))

def Umaury4(n):
    n = np.asarray(n, dtype=float)
    return np.piecewise(n, [np.logical_and(n <= 20, n >=2), n < 2],
                        [ lambda n: Emaury[n.astype(int) - 2, 0],
                         float('nan'),
                         lambda n: CPfit4(n, *maury_fit4)])

def Umaury3(n):
    n = np.asarray(n, dtype=float)
    return np.piecewise(n, [np.logical_and(n <= 20, n >=2), n < 2],
                        [ lambda n: Emaury[n.astype(int) - 2, 0],
                         float('nan'),
                         lambda n: CPfit3(n, *maury_fit3)])

def Umaury_random(n):
    n = np.asarray(n, dtype=float)
    Erandom = rng.normal(Emaury[:, 0], scale=Emaury[:, 1])
    random_fit = rng.normal(maury_fit3, maury_dfit)
    return np.piecewise(n, [np.logical_and(n < 20, n >=2), n < 2],
                        [ lambda n: Erandom[n.astype(int) - 2],
                         float('nan'),
                         #lambda n: Emaury[-1] -(Emaury[-2] - Emaury[-1]) * (n - 20)])
                         lambda n: CPfit(n, *random_fit)])

def Upower(n, A=maury_fit3[0], p=4):
#def Upower(n, A=-8.142e4*2, p=4.7):
    return A / n**p - n * νB
    

def gauss_cloud(x, x0, σ):
    return 1 / np.sqrt(2 * np.pi * σ**2) * np.exp(- (x - x0)**2 / (2 * σ**2))


def P_interf(δ, Ω0, τ, T, δAC):
    """
    Probabilité d'excitation d'un interféromètre π/2 - π/2, en présence d'un LS raman 
    différentiel δAC 
     """

    φ = 0
    Ωr = np.sqrt((δ - δAC)**2 + Ω0**2)
    return 4 * Ω0**2 / Ωr**2 * np.sin(Ωr * τ / 2)**2 \
        * (np.cos(Ωr * τ / 2) * np.cos((δ * T + φ) / 2) - (δ - δAC) / Ωr * np.sin(Ωr * τ / 2) * np.sin((δ * T + φ) / 2))**2

class CP:
    def __init__(self, σz, m=6, tau_pi=22e-3, T=.1, δAC=0, 
                 ):
        self.σz = σz
        self.m = m
        self.tau_pi = tau_pi
        self.T = T
        self.δAC = δAC


    def __call__(self, xc, midfringe_method='integrate', U=Umaury3):
        match midfringe_method:
            case 'integrate':
                midfringe = self.integrate_fringe
            case 'minimize_scalar':
                midfringe = self.minimize_scalar
            case _:
                raise ValueError(f'Mid fringe determination method "{midfringe_method}" is not known')

        return self.meanCP_bloch(xc, midfringe, U)

    def meanCP_bloch(self, xc, midfringe_method, U):
        """
        Force de CP moyenne mesurée en faisant deux mesures à ±m transitions, moyennée sur la taille du nuage
        """
        f1 = self.meanCP_f0(self.m, xc, midfringe_method, U)
        f2 = self.meanCP_f0(-self.m, xc, midfringe_method, U)

        f_bloch = (f2 - f1) / (2 * self.m)
        return f_bloch

    def meanCP_f0(self, m, xc, midfringe_method, U):
        """
        Fréquence centrale d'un interféromètre sous l'influnce de la force de CP
        """

        f0 = U(1000 + m) - U(1000)
        xc = np.atleast_1d(xc)
        fCP = np.zeros(len(xc))

        for ix0 in (np.argsort(xc)[::-1]):
            x0 = xc[ix0]
            Pe = lambda f: self.meanCP_interf(f, m, x0, U)
            f0 = midfringe_method(Pe, f0)
            fCP[ix0] = f0

        return fCP


    def meanCP_interf(self, f, m, xc, U): 
        nrange = np.arange(2 - min(0, m), int((np.max(xc) + 3 * self.σz) / a))[None, :]
        μ = gauss_cloud(nrange * a, xc, self.σz)

        f = np.atleast_1d(f)
        #δ = 2 * np.pi * (f[:, None] - np.abs(U(nrange + m) - U(nrange)) * -np.sign(m))
        δ = 2 * np.pi * (f[:, None] - (U(nrange + m) - U(nrange)))
        Pe = P_interf(δ, np.pi / self.tau_pi, self.tau_pi/2, self.T, self.δAC * 2 * np.pi)

        return np.sum(Pe * μ, -1) / np.sum(μ)

    def integrate_fringe(self, Pe, f0, N=30):
        """
        Intégration de la frange centrale
        """
        sweep = 1/(self.T + 2 * self.tau_pi / np.pi) / 4
        G = 1 / (4 * np.pi / (sweep * 4))
        Pe0 = Pe(f0 + sweep)
        f = np.empty(N)
        for i in range(N):
            f[i] = f0
            Pe0, Pe1 = Pe(f0 - (-1)**i * sweep), Pe0
            f0 -= (Pe0 - Pe1) * G * (-1)**i

        return f0

    def minimize_scalar(self, Pe, f0):
        sweep = 1/(self.T + 2 * self.tau_pi / np.pi) / 4
        return minimize_scalar(lambda f: -Pe(f), bounds=(f0 - sweep, f0 + sweep)).x
        return minimize_scalar(lambda f: -Pe(f), (f0 - sweep, f0, f0 + sweep)).x

def fit():
    d = np.linspace(0, 20e-6, 20)
    C = CP(3e-6, m=6)
    CPm = C(d, 'minimize_scalar')
    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.clf()
    plt.plot(d * 1e6, CPm - νB) 
    plt.plot(d * 1e6, C(d, 'minimize_scalar', U=Upower) - νB) 
    fit = (maury_fit3[0], 4)
    fit, dfit = curve_fit(lambda d, A, p: C(d, 'minimize_scalar', U=lambda n: Upower(n, A, p)) - νB, d, CPm - νB, p0 = (maury_fit3[0], 4))
    plt.plot(d * 1e6, C(d, 'minimize_scalar', U=lambda n: Upower(n, *fit)) - νB , ':')
    print(fit)

    plt.figure(2)
    plt.clf()
    n = np.arange(30)
    plt.loglog(n, np.abs(Umaury(n + 1) - Umaury(n) + νB), '+')
    plt.loglog(n, np.abs(Upower(n + 1) - Upower(n) + νB), '+')
    plt.loglog(n, np.abs(Upower(n + 1, *fit) - Upower(n, *fit) + νB), '+')

             
import matplotlib.pyplot as plt
def fit2():
    d = np.linspace(0, 20e-6, 30)
    C = CP(.1e-6, m=1)
    plt.figure(1)
    plt.clf()
    plt.plot(d * 1e6, C.meanCP_f0(-6, d, C.integrate_fringe, Upower))
    plt.plot(d * 1e6, np.abs(C.meanCP_f0(6, d, C.integrate_fringe, Upower)))

    plt.figure(2)
    plt.clf()
    f = np.linspace(3360, 3460, 500)
    plt.plot(f, C.meanCP_interf(-f, 6, 0, Upower))
    plt.plot(f, C.meanCP_interf(-f, 6, 2.5e-6, Upower))
    plt.plot(f, C.meanCP_interf(-f, 6, 7.5e-6, Upower))

    plt.figure(3)
    plt.clf()

    δ = 2 * np.pi * (-f[:, None] - (Upower(10 + 6) - Upower(10)))
    plt.plot(f, P_interf(δ, np.pi / C.tau_pi, C.tau_pi/2, C.T, C.δAC * 2 * np.pi))
    

μ = 3.2 * 3.33e-30 # Moment dipolaire du Rb sur de la silice
α0 = .079e-4
def gauss_dip(d, N, σ0) :
    #A = np.exp(d**2 / (2 * σ0**2)) * erfc(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    A = erfcx(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    dE = N**2 * μ**2 * (-2 * d * σ0 + A * (d**2 + σ0**2)) * (-2 * σ0 * (d**2 + 2 * σ0**2) + d * A * (d**2 + 3 * σ0**2)) / (64 * np.pi**2 * Rb.ε0**2 * σ0**12)
    F = Rb.h * α0 * dE

    return F * 532e-9 / (2 * Rb.h) #- gauss_dip(350e-6, N, σ0)

def Egauss(d, N, σ0) :
    A = erfcx(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    E = N * μ * (-2 * d * σ0 + A * (d**2 + σ0**2)) / (8 * np.pi * Rb.ε0 * σ0**5)

    return E

def dEgauss(d, N, σ0) :
    """
    Gradient de champ électrique
    """
    #A = np.exp(d**2 / (2 * σ0**2)) * erfc(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 

    A = erfcx(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    dE = N * μ * (-2 * σ0 * (d**2 + 2 * σ0**2) + d * A * (d**2 + 3 * σ0**2)) / (8 * np.pi * Rb.ε0 * σ0**7)

    return dE


def FEgauss(d, N, σ0):
    F = Rb.h * α0 * Egauss(d, N, σ0) * dEgauss(d, N, σ0)
    return F * 532e-9 / (2 * Rb.h) #- gauss_dip(350e-6, N, σ0)

def dFEgauss(d, N, σ0, dN, dσ0, cov):
    """
    Incertitude sur la valeur de la force en fonction des incertitudes des fits
    """
    dFN = 2 / N * FEgauss(d, N, σ0)
    A = erfcx(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    dFσ = -(N * μ / (4 * np.pi * Rb.ε0))**2 * (2 * d * σ0**2 * (d**4 + 8 * d**2 * σ0**2 + 9 * σ0**4) - A * σ0 * (2 * d**6 + 18 * d**4 * σ0**2 + 31 * d**2  * σ0**4 + 7 * σ0**6)
                                                 + d * A**2 / 2 * (d**6 + 10 * d**4 * σ0**2 + 23 * d**2 * σ0**4 + 12 * σ0**6)) / σ0**15
    dFσ *= Rb.h * α0 * 532e-9 / (2 * Rb.h)

    return np.sqrt(dFN**2 * dN**2 + dFσ**2 * dσ0**2 + 2 * cov * dFσ * dFN)

def dFEgauss_Eext(d, N, σ0, cov, Eext=0):
    """
    Incertitude sur la valeur de la force en fonction des incertitudes des fits
    cov = [[σN**2, Cov(N, σ)],
           [Cov(N, σ), σσ0**2]]
    """
    dFN = 2 / N * FEgauss(d, N, σ0)
    EN = Egauss(d, N, σ0) / N
    dEN = dEgauss(d, N, σ0) / N

    dFN = EN * dEgauss(d, N, σ0) + (dEN * (Egauss(d, N, σ0) + Eext))

    A = erfcx(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    Eσ = - N * μ * (-2 * d * σ0 * (d**2 + 5 * σ0**2) + A * (d**4 + 6 * d**2 * σ0**2 + 3 * σ0**4)) / (8 * Rb.ε0 * np.pi * σ0**8)
    dEσ = N * μ * (2 * σ0 * (d**4 + 9 * d**2 * σ0**2 + 8 * σ0**4) - A * (d**5 + 10 * d**3 * σ0**2 + 15 * d * σ0**4)) / (8 * Rb.ε0 * np.pi * σ0**10)

    dFσ = Eσ * dEgauss(d, N, σ0) + (dEσ * (Egauss(d, N, σ0) + Eext))

    return np.sqrt(dFN**2 * cov[0, 0] + dFσ**2 * cov[1, 1] + 2 * cov[1, 0] * dFσ * dFN) * Rb.h * α0 * 532e-9 / (2 * Rb.h)

def σdEgauss(d, N, σ0, cov) :
    """
    Incertutitude sur le gradient de champ électrique
    """

    dEN = dEgauss(d, N, σ0) / N

    A = erfcx(d / (np.sqrt(2) * σ0)) * np.sqrt(2 * np.pi) 
    dEσ = N * μ * (2 * σ0 * (d**4 + 9 * d**2 * σ0**2 + 8 * σ0**4) - A * (d**5 + 10 * d**3 * σ0**2 + 15 * d * σ0**4)) / (8 * Rb.ε0 * np.pi * σ0**10)

    return np.sqrt(dEN**2 * cov[0, 0] + dEσ**2 * cov[1, 1] + 2 * cov[1, 0] * dEσ * dEN) 


def _gauss_dip_z_mean(d, N, σ0, σc, FE=FEgauss):
    low_lim = np.maximum(0, d - 4 * σc)
    if low_lim > 0:
        norm = 1
    else:
        norm = quad(lambda z: 1 / np.sqrt(2 * np.pi * σc**2) * np.exp(-(z - d)**2 / (2 * σc**2)),
                low_lim, d + 4 * σc)[0]
            
    return quad(lambda z: 1 / np.sqrt(2 * np.pi * σc**2) * np.exp(-(z - d)**2 / (2 * σc**2)) * FE(z, N, σ0),
                low_lim, d + 4 * σc)[0] / norm

gauss_dip_z_mean = np.vectorize(_gauss_dip_z_mean, otypes=[float])

#@vectorize([float64(float64, float64, float64, float64, float64)], nopython=True)
def E2Dz_jit(z, x, N, σad, x0=0): 
    """
    Intégrale du champ dipolaire sur une distribution gaussienne σad sur y, 
    pour un champ en (x0, z)
    """
    return np.exp(- x**2 / (2 * σad**2)) * \
             ((z**2 + σad**2) * kve(float64(0), ((x-x0)**2 + z**2) / (4 * σad**2)) -
              (z**4 - z**2 * σad**2 + (x-x0)**2 * (z**2 + σad**2)) * kve(float64(1), ((x-x0)**2 + z**2) / (4 * σad**2)) / ((x-x0)**2 + z**2)) \
        * N * μ / (16 * np.pi**2 * σad**6 * Rb.ε0)

def E2Dz(z, x, N, σad, x0=0): 
    """
    Intégrale du champ dipolaire sur une distribution gaussienne σad sur y, 
    pour un champ en (x0, z)
    """
    return np.exp(- x**2 / (2 * σad**2)) * \
             ((z**2 + σad**2) * kve(0, ((x-x0)**2 + z**2) / (4 * σad**2)) -
              (z**4 - z**2 * σad**2 + (x-x0)**2 * (z**2 + σad**2)) * kve(1, ((x-x0)**2 + z**2) / (4 * σad**2)) / ((x-x0)**2 + z**2)) \
        * N * μ / (16 * np.pi**2 * σad**6 * Rb.ε0)


def E2Dx(z, x, N, σad, x0=0): 
    """
    Intégrale du champ dipolaire sur une distribution gaussienne σad sur y, 
    pour un champ en (x0, z)
    """
    return - np.exp(- x**2 / (2 * σad**2)) * (x - x0) * z * \
             (kve(0, ((x-x0)**2 + z**2) / (4 * σad**2)) -
              ((x-x0)**2  + z**2 - 2 * σad**2) * kve(1, ((x-x0)**2 + z**2) / (4 * σad**2)) / ((x-x0)**2 + z**2))\
        * N * μ / (16 * np.pi**2 * σad**6 * Rb.ε0)

#@vectorize([float64(float64, float64, float64, float64, float64)], nopython=True)
def dE2Dz_jit(z, x, N, σad, x0=0):
    return np.exp(-x**2 / (2 * σad**2)) * N * z * μ * (
        ((x - x0)**2 + z**2) * ((x - x0)**2 * z**2 + z**4 + (3 * (x - x0)**2 + 2 * z**2) * σad**2) * kve(0., ((x-x0)**2 + z**2) / (4 * σad**2)) -
        (((x - x0)**2 * z + z**3)**2 + 3 * (x - x0)**2 * ((x-x0)**2 + z**2) * σad**2 +  
         2 * (-3 * (x-x0)**2 + z**2) * σad**4) * kve(1., ((x-x0)**2 + z**2) / (4 * σad**2))) / (
             16 * np.pi**2 * ((x-x0)**2 + z**2)**2 * Rb.ε0 * σad**8) 

    return 3 * np.exp(-x**2 / (2 * σad**2)) * z * (
        2 * (3 * (x - x0)**2 - 2 * z**2) * hyperu(1/2, -2., ((x-x0)**2 + z**2) / (2 * σad**2))
        + 3 * ((x - x0)**2 + z**2) * hyperu(3/2, -1., ((x-x0)**2 + z**2) / (2 * σad**2))) / (
            16 * np.pi**(3/2) * ((x-x0)**2 + z**2)**3 * Rb.ε0 * σad**2) * N * μ

def dE2Dz(z, x, N, σad, x0=0):
    #return np.exp(-x**2 / (2 * σad**2)) * N * z * μ * (
    #    ((x - x0)**2 + z**2) * ((x - x0)**2 * z**2 + z**4 + (3 * (x - x0)**2 + 2 * z**2) * σad**2) * kve(0, ((x-x0)**2 + z**2) / (4 * σad**2)) -
    #    (((x - x0)**2 * z + z**3)**2 + 3 * (x - x0)**2 * ((x-x0)**2 + z**2) * σad**2 +  
    #     2 * (-3 * (x-x0)**2 + z**2) * σad**4) * kve(1, ((x-x0)**2 + z**2) / (4 * σad**2))) / (
    #         16 * np.pi**2 * ((x-x0)**2 + z**2)**2 * Rb.ε0 * σad**8) 

    # Version hyperu plus simple, mais scipy mais 10 fois plus de temps à la calculer
    return 3 * np.exp(-x**2 / (2 * σad**2)) * z * (
        2 * (3 * (x - x0)**2 - 2 * z**2) * hyperu(1/2, -2., ((x-x0)**2 + z**2) / (2 * σad**2))
        + 3 * ((x - x0)**2 + z**2) * hyperu(3/2, -1., ((x-x0)**2 + z**2) / (2 * σad**2))) / (
            16 * np.pi**(3/2) * ((x-x0)**2 + z**2)**3 * Rb.ε0 * σad**2) * N * μ

def dE2Dx(z, x, N, σad, x0=0):
    return np.exp(-x**2 / (2 * σad**2)) * N * (x - x0) * μ * (
        - ((x - x0)**2 + z**2) * ((x - x0)**2 * z**2 + z**4 + (x - x0)**2 * σad**2) * kve(0, ((x-x0)**2 + z**2) / (4 * σad**2)) +
        (((x - x0)**2 * z + z**3)**2 + ((x - x0)**2 - 2 * z**2) * ((x-x0)**2 + z**2) * σad**2 -  
         2 * ((x-x0)**2 - 3 * z**2) * σad**4) * kve(1, ((x-x0)**2 + z**2) / (4 * σad**2))) / (
             16 * np.pi**2 * ((x-x0)**2 + z**2)**2 * Rb.ε0 * σad**8) 

    return 3 * np.exp(-x**2 / (2 * σad**2)) * (x - x0) * (
        2 * ((x - x0)**2 - 4 * z**2) * hyperu(1/2, -2., ((x-x0)**2 + z**2) / (2 * σad**2))
        + ((x - x0)**2 + z**2) * hyperu(3/2, -1., ((x-x0)**2 + z**2) / (2 * σad**2))) / (
            16 * np.pi**(3/2) * ((x-x0)**2 + z**2)**3 * Rb.ε0 * σad**2) * N * μ


def _F2D(z, N, σad, x0=0):
    Fx = E2D_tot(z, N, σad, x0, E=E2Dx) * E2D_tot(z, N, σad, x0, E=dE2Dx)
    Fz = E2D_tot(z, N, σad, x0, E=E2Dz) * E2D_tot(z, N, σad, x0, E=dE2Dz)
    return (Fx + Fz) * α0 * 266e-9

F2D = np.vectorize(_F2D, otypes=[float])

def _E2D_tot(z, N, σad, x0=0, E=dE2Dz):

    #d = np.linspace(-5 * σad, 5 * σad, 500)
    #f = E(z, d, N, σad, x0)
    #return trapezoid(f, d)

    A = 1e-8
    R = quad(lambda x: E(z, x, N, σad, x0) * A, -5 * σad, 5 * σad, full_output=True, epsabs=0, epsrel=1e-3)
    return R[0] / A
E2D_tot = np.vectorize(_E2D_tot, otypes=[float])

def _F2D_mean(z, N, σad, σr):
    R = quad(lambda x0: 2 * np.pi * x0 * np.exp(-x0**2 / (2 * σr**2)) * _F2D(z, N, σad, x0) / (2 * np.pi * σr**2),
                0, 3 * σr, epsabs=0, epsrel=1e-8, full_output=True)
    return R[0]

F2D_mean = np.vectorize(_F2D_mean, otypes=[float])

def _E2D_mean(z, N, σad, σr, E=dE2Dz):
    R = quad(lambda x0: 2 * np.pi * x0 * np.exp(-x0**2 / (2 * σr**2)) * E2D_tot(z, N, σad, x0, E) / (2 * np.pi * σr**2),
                0, 3 * σr, epsabs=0, epsrel=1e-8, full_output=True)
    return R[0]

E2D_mean = np.vectorize(_E2D_mean, otypes=[float])

def φ(ε):
    """Effect of the Casimir-Polder force on the collective oscillations of a trapped Bose-Einstein condensate"""
    return (ε + 1) / (ε - 1) * (
        1/3 + ε + (4 - (ε + 1) * np.sqrt(ε)) / (2 * (ε - 1))
        - np.arcsinh(np.sqrt(ε-1)) * (1 + ε + 2 * ε * (ε-1)**2) / (2 * (ε-1)**(3/2))
        + ε**2 * (np.arcsinh(np.sqrt(ε)) - np.arcsinh(1 / np.sqrt(ε))) / np.sqrt(ε + 1)
    )
