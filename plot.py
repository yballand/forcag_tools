import argparse
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd 

def plot_rabi(files):
    files = tuple(files)

    if files:
        plt.figure()
        for file in files: 
            try:
                rabi_data = pd.read_csv(file, sep=' ', dtype=np.float64)
                rabi_data = rabi_data[rabi_data.Ntot > 1.5e5]
                print(rabi_data)
            except ValueError as e:
                # Bad formated file. Must be something else
                print(e)
                pass 
            else: 
                rabi_data_mean = rabi_data.groupby('freqtime').mean()
                rabi_data_mean.ratio = rabi_data_mean.N0 / rabi_data_mean.Ntot

                plt.plot(rabi_data_mean.index, rabi_data_mean.ratio, label=Path(file).stem)

        plt.legend()
        plt.xlabel('f/t')
        plt.ylabel('ratio')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Quicky plot Rabi')
    parser.add_argument('rabi_files', metavar='Rabi files', nargs='+')
    args = parser.parse_args()

    path_file = []
    for file in args.rabi_files:
        path = Path(file)
        if path.is_dir():
            # on trace tous les rabi du dossier
            plot_rabi(path.glob('*.rabitau'))
            plot_rabi(path.glob('*.rabifreq'))

        else:
            path_file.append(path)

    # On trace les fichiers restants
    plot_rabi(path_file)

    plt.show()
