from collections import namedtuple
import operator
import struct

from pycparser import parse_file, c_ast
import jinja2

PYTYPES = {"short": 'T_SHORT',
           "int": 'T_INT',
           "long": 'T_LONG',
           "unsigned short": 'T_USHORT',
           "unsigned int": 'T_UINT',
           "unsigned long": 'T_ULONG',
           "double": 'T_DOUBLE',
           "float": "T_FLOAT", 
           "char": "T_CHAR",
           "unsigned char": "T_UBYTE",
           "struct": "T_STRUCT"}

WARNING = """/* This is a code automatically gererated from {template}.
   Any modification may be automatically deleted */

"""

OP = {'-': operator.sub}

STRUCT_CHAR = {
    'short': 'h', 
    'int': 'i',
    'long': 'i',
    'unsigned short': 'H',
    'unsigned int': 'I', 
    'unsigned long': 'I', # beware of the OS. On XP, long ~ int, but maybe not true on windows 10 64 bit
    'double': 'd', 
    'float': 'f',
    'unsigned char': 'B',
    'char': 's',
}

STRUCT_ALIGNMENT = { # construct type, alignement
    'short': 2, 
    'int': 4,
    'long': 4,
    'unsigned short': 2,
    'unsigned int': 4, 
    'unsigned long': 4, # 8 bytes on 64-bit
    'double': 8, 
    'float': 4,
    'unsigned char': 1,
    'char': 1,
}

WFMs = {'wfm_selection2',
        'wfm_detection',
        'wfm_molasses',
        'wfm_mot',
        'wfm_postcooling',
        'wfm_postcooling_time_constant',
        'wfm_raman',
        'wfm_selection',
        'wfm_selection2'}

WFM = namedtuple('WFM', ['a', 'IR', 'b', 'detuning', 'raman', 'c', 'Verdi', 'AOM_cooling'])


def parse_c():
    ast = parse_file("../Definitions.h", use_cpp=True, cpp_args=['-E'],
                     #cpp_path='C:/Program Files/Microsoft Visual Studio/2022\Community/VC/Tools/Llvm/x64/bin/clang.exe')
                     cpp_path='clang')
    #ast = parse_file("Definitions.pre")
    return ast 


class FindStruct(c_ast.NodeVisitor):
    def __init__(self):
        """
        Define a visitor which will find a given struct
        """
        self.struct_name = None
        self.result = None

    def find(self, ast, struct_name):
        """
        Find in `ast` a struct with name `struct_name`
        """
        self.struct_name = struct_name
        self.visit(ast)
        return self.result

    def visit_Struct(self, node):
        if node.name == self.struct_name:
            self.result = node

############################################
# Classes for representation of the declarations ast

class Struct:
    def __init__(self, type, name=None):
        """
        C struct declaration representation
        """
        self.base_type = type

        self.type = type
        if type is not None:
            self.type = "struct " + type

        self.name = name
        self.children = []
        self.arrays = []
        self.simple_members = []
        self.structs = []
    
    def __repr__(self):
        str_member = "\n".join(str(a) for a in self.children)
        # Rajoute une tabulation à toutes les lignes 
        str_member = "\n\t".join(l for l in str_member.splitlines())
        return "Struct({name},\n\t{member}\n)".format(name=self.name, member=str_member)
    
    def add_field(self, field):
        if field.type: # TODO
            self.children.append(field)

            if isinstance(field, Decl):
                    self.simple_members.append(field)
            elif isinstance(field, Array):
                    self.arrays.append(field)
            elif isinstance(field, Struct):
                    self.structs.append(field)

    @property
    def ctype(self):
        return C_filter(self.base_type)

    def struct_format(self):
        """
        Format struct to represent in the struct module the binary reprensation
        For a struct, beware of padding and alignment.
        """
        format_list = []
        size = 0
        for e in self.children:
            alignment = e.struct_alignment
            if size % alignment:
                padding = alignment - size % alignment 
                format_list.append(f"{padding}x")
                size += padding

            format_list.append(e.struct_format())
            size += struct.calcsize(format_list[-1])

        # Final alignement of the struct
        if size % self.struct_alignment:
            padding = self.struct_alignment - size % self.struct_alignment 
            format_list.append(f"{padding}x")

        return ''.join(format_list)

    def struct_result(self, data_parse_reverse):
        """
        Proper formating of the parse result of a sruct.
        Return a namedtuple with named fields corrreponding to the fields of the struct.

        data_parse_reverse must be in reverse order, eg. the first field as the last element
        """
        name = self.name if self.name is not None else 'hardware_config'
        tuple_r = namedtuple(name, [e.name for e in self.children])
        list_elements = [e.struct_result(data_parse_reverse) for e in self.children]

        return tuple_r(*list_elements)

    @property
    def struct_alignment(self):
        """
        The number of bytes required for a proper alignement
        """
        return max(e.struct_alignment for e in self.children)


class Decl:

    def __init__(self, type, name):
        """
        Declaration of a simple C element
        """
        self.name = name
        self.type = type
        self.len = 1

        self.struct_alignment = STRUCT_ALIGNMENT[self.type]

    def __repr__(self):
        return f"{self.type} {self.name}"

    def cpython_member(self):
        py_type = PYTYPES[self.type]
        member = f"""{{"{self.name}", {py_type}, offsetof(HarwareConfigObject, hardware_config.{self.name}), 0, "{self.name}"}}"""
        return member

    @property
    def py_type(self):
        return PYTYPES[self.type]

    def struct_format(self):
        return STRUCT_CHAR[self.type]

    def struct_result(self, data_parse_reverse):
        if data_parse_reverse:
            return data_parse_reverse.pop()
        return None

    
class Array:
    def __init__(self, element, n_elements):
        self.element = element
        self.name = element.name
        self.type = element.type
        self.len = int(n_elements)

    def __repr__(self):
        return f"{self.element}[{self.len}]"

    @property
    def py_type(self):
        return PYTYPES[self.type]

    def struct_format(self):
        return self.element.struct_format() * self.len

    def struct_result(self, data_parse_reverse):
        if self.type != 'char':
            tuple_result = tuple(self.element.struct_result(data_parse_reverse) for i in range(self.len))
            if self.name not in WFMs:
                return tuple_result
            else:
                return WFM._make(tuple_result)


        else:
            return b''.join(self.element.struct_result(data_parse_reverse) for i in range(self.len)).rstrip(b'\x00')
    
    @property
    def struct_alignment(self):
        return self.element.struct_alignment


#####################
# Sanitize types 
class SanitizeType:
    """
    Sanitize types
    Either by removing synonyms (signed long int == long) or by replacing types defined in a typedef
    """
    def __init__(self, file_ast):
        self.file_ast = file_ast
        self.typedef_subst = {}
        self.typedef_struct = {}

        self.typedef(file_ast)


    def typedef(self, file_ast):
        """
        Parse the full ast of file looking for typedef.
        Either typedef for synonyms (go in typedecl) or for struct directly declared inside a typedef
        (go in typedef_struct)
        """
        for node in file_ast:
            if isinstance(node, c_ast.Typedef):
                if (isinstance(node.type.type, c_ast.IdentifierType)):
                    type_decl = node.type
                    self.typedef_subst[type_decl.declname] = " ".join(type_decl.type.names)
                elif (isinstance(node.type.type, c_ast.Struct)):
                    self.typedef_struct[node.name] = node.type.type

    def __call__(self, decl_name, type_name):
        while (type_name in self.typedef_subst):
            type_name = self.typedef_subst[type_name]

        if type_name in self.typedef_struct:
            struct_r = parse_struct_ast(self.typedef_struct[type_name], self.file_ast)
            struct_r.name = decl_name
            struct_r.type = type_name
            PYTYPES[type_name] = PYTYPES["struct"]

            return struct_r

        # Remove some synonyms
        prefix = "signed "
        if type_name.startswith(prefix):
            type_name = type_name[len(prefix):]

        type_name = type_name.replace("short int", "short")
        type_name = type_name.replace("long int", "long")

        return Decl(type_name, decl_name)

def parse_struct_ast(struct_ast, file_ast):
    """
    Parse the `struct` ast from pycparser to generate a structure with custom types 
    (Struct(), Array(), etc.)
    """
    sanitize = SanitizeType(file_ast)

    #struct_ast = FindStruct().find(file_ast, struct_name) 
    struct_r = Struct(struct_ast.name, None)

    for decl in struct_ast.decls:
        name = decl.name
        if (isinstance(decl.type, c_ast.TypeDecl) and
                isinstance(decl.type.type, c_ast.IdentifierType)):
            type_decl = " ".join(decl.type.type.names)
            struct_r.add_field(sanitize(name, type_decl))

        elif (isinstance(decl.type, c_ast.TypeDecl) and
                isinstance(decl.type.type, c_ast.Struct)):

            ctype = decl.type.type.name
            struct_ast = FindStruct().find(file_ast, ctype)
            substruct = parse_struct_ast(struct_ast, file_ast)
            substruct.name = name
            struct_r.add_field(substruct)

        elif (isinstance(decl.type, c_ast.ArrayDecl)):
            array_decl = decl.type.type
            identifier = array_decl.type

            name = array_decl.declname
            type_array = " ".join(identifier.names)
            # Si jamais la dimension est une opération
            dim = decl.type.dim
            match dim:
                case c_ast.BinaryOp():
            #if isinstance(dim, c_ast.BinaryOp):
                    dim_nb = OP[dim.op](float(dim.left.value), float(dim.right.value))
                case c_ast.ID(name='MAX_PATHNAME_LEN'):
                    dim_nb = 260
                case _:
                    dim_nb = dim.value
               
            struct_r.add_field(Array(sanitize(name, type_array), dim_nb))
        else:
            raise ValueError('Error : unknown field')

    return struct_r

def generate_c(hardware_struct, template="python_ext_type_template", dest="../src/python_ext_type"):
    template_env = jinja2.Environment(loader=jinja2.FileSystemLoader('.'),
                                      comment_start_string='{##', 
                                      trim_blocks=True)

    template_env.filters['C'] = C_filter
    template_c = template_env.get_template(template + '.c')
    template_h = template_env.get_template(template + '.h')
    context = {'hardware': hardware_struct, 
               'max_structs': 10} # TODO

    with open(dest + '.c', 'w') as f:
        f.write(WARNING.format(template=template + '.c'))
        f.write(template_c.render(**context))

    with open(dest + '.h', 'w') as f:
        f.write(WARNING.format(template=template + '.h'))
        f.write(template_h.render(**context))

def C_filter(string):
    return ''.join(word.title() for word in string.split('_'))


def parse_struct(filename, hardware_struct=None):
    """
    Parse the binary file `filename` which is a dump of a hardware_config struct
    """

    if not hardware_struct:
        hardware_struct = get_struct()

    format = '<' + hardware_struct.struct_format()

    with open(filename, 'rb') as f:
        data = f.read()

    unpacked = struct.unpack(format, data)

    return hardware_struct.struct_result(list(unpacked[::-1]))

def get_struct():
    ast = parse_c()
    hardware_ast = FindStruct().find(ast, 'hardware_config_type')
    hardware_struct = parse_struct_ast(hardware_ast, ast)
    return hardware_struct

if __name__ == "__main__":
    ast = parse_c()

    hardware_ast = FindStruct().find(ast, 'hardware_config_type')
    hardware_struct = parse_struct_ast(hardware_ast, ast)

    generate_c(hardware_struct)




