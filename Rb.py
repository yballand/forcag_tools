from scipy import constants

c = constants.c
h = constants.h
hbar = constants.hbar
kB = kb = constants.Boltzmann
σ = sigmaF = constants.sigma     # Stefan

mu_0 = mu0 = μ0 = constants.mu_0
epsilon_0 = ε0 = constants.epsilon_0
m = 1.44316060e-25
λ = 780.241e-9

g = 9.81
α0 = h * 0.0794e-4
ks = -1.24e-10 # Hz / (V / m)^2

cross_section = 3 * λ**2 / (2 * constants.pi)

