#!/usr/bin/python3
import argparse
import csv

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import pandas as pd

def read_file(f, z_col, w_col):
    data = pd.read_csv(f, quoting=csv.QUOTE_ALL, usecols=[z_col, w_col],
                        names=('z', 'w'), header=0)
    data.z *= 1e-3 # en m
    data.w *= 1e-6 / 2

    return data

def beam_radius(z, w0, zw, λ):
    z0 = np.pi * w0**2 / λ

    return w0 * np.sqrt(1 + ((z - zw)/z0)**2)


def fit_data(data, λ=780e-9):
    z = data.z
    w = data.w

    p0 = (np.min(w), -30e-2)
    return curve_fit(lambda z, w, zw: beam_radius(z, w, zw, λ=λ),
                     z, w, p0=p0)


def plot_fit(data, fit_result, λ=780e-9):
    plt.figure(1)
    plt.clf()
    z = data.z * 1e2
    w = data.w * 1e6
    plt.plot(z, w, '+', label='données') 

    zw = fit[0][1] * 1e2
    zlin = np.linspace(min(zw, np.min(z)), max(zw, np.max(z)), 200)
    plt.plot(zlin, beam_radius(zlin * 1e-2, *fit_result[0], λ) * 1e6, label='fit')
    plt.ylabel('Rayon du faisceau (µm)')
    plt.xlabel('z (cm)')
    plt.legend()
    plt.title(f"Fit du rayon d'un faisceau gaussien (λ={λ*1e9:.0f} nm)")

    plt.show()


def arg_parse(*args):
    parser = argparse.ArgumentParser(description='Fit measured beam radius to find its waist')

    parser.add_argument('file', help="Data file. First column must be a distance in cm, the second the radius in µm.", metavar='data file')
    parser.add_argument('-λ', '--lambda', type=float, help="Laser wavelength in nm (default 780)", default=780, 
                        metavar='lambda', dest='λ') 
    parser.add_argument('-z', '--z_col', type=int, help="Column number of z position", default=0)
    parser.add_argument('-w', '--w_col', type=int, help="Column number of waist radius", default=1)



    if not args:
        args = None
    return parser.parse_args(args)

if __name__ == '__main__': 
    args = arg_parse()

    assert args.z_col != args.w_col

    data = read_file(args.file, z_col=args.z_col, w_col=args.w_col)

    fit = fit_data(data, args.λ * 1e-9)

    print(f"Waist du faisceau : {fit[0][0] * 1e6:.4}±{fit[1][0, 0] * 1e6:.2} μm")
    print(f"Position du waist : {fit[0][1] * 1e2:.4}±{fit[1][1, 1] * 1e2:.2} cm")
    plot_fit(data, fit, args.λ * 1e-9)




